<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('image');
            $table->integer('quantity');
            $table->string('size');
            $table->string('color');
            $table->string('price');
            $table->string('old_price');
            $table->string('available_size');
            $table->string('available_color');
            $table->string('stock');
            $table->string('pro_available');
            $table->string('dis_available');
            $table->string('tags');
            $table->string('brand');
            $table->string('delivery');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
