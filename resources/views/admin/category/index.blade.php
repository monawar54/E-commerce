@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper" style="padding-left: 15px">
    <div class="right_col" role="main">
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Category Content </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> Category Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @php $i = 1; @endphp
                        @foreach($category as $sub_categories)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$sub_categories->catName}}</td>
                                {{--@foreach($subcat as $sub_categories)
                                    <td>{{$sub_categories->sub_category}}</td>
                                @endforeach--}}
                                <td>
                                    <a class="btn btn-danger" href="{{ url('/admin/category/'. $sub_categories->id) }}"> Delete </a>
                                    <a class="btn btn-primary" href="{{ url('/admin/category/edit/'. $sub_categories->id) }}"> Edit </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection