
@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel logo_form">
                    <div class="x_title">
                        <h2>Slider table </h2>
                        <div class="clearfix"></div>
                    </div>

                    <br />
                    <hr>

                    <div class="x_content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Sl.</th>
                                <th> Title </th>
                                <th> Description </th>
                                <th>Images</th>
                                <th>Logo</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php $i = 1; @endphp
                            @foreach($sliders as $slider)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{ substr($slider->title, 0,30) }}</td>
                                    <td>{{ substr($slider->description, 0,30) }}</td>
                                    <td>
                                        <img width="100" src="{{asset('uploads/'.$slider->image)}}" alt="">
                                    </td>
                                    <td>
                                        <img width="50" src="{{asset('uploads/'.$slider->logo)}}" alt="">
                                    </td>
                                    <td >
                                        <div class="btn-group  btn-group-sm" style="color: white">
                                            <button class="btn btn-success" type="button"> <a href="{{ url('/admin/slider/show/'. $slider->id) }}" style="color: white"><i class="fa fa-eye"></i></a></button>
                                            <button class="btn btn-primary" type="button"><a href="{{ url('/admin/slider/edit/'. $slider->id) }}" style="color: white"><i class="fa fa-edit"></i></a></button>
                                            <button class="btn btn-danger" type="button"><a href="{{ url('/admin/slider/delete/'. $slider->id) }}" style="color: white"><i class="fa fa-trash"></i></a></button>
                                        </div>
                                    </td>
                                </tr>
                                @php $i++; @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()