@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel logo_form">
                    <div class="x_title">
                        <h2>Team Member table </h2>
                        <div class="clearfix"></div>
                    </div>

                    <br />
                    <hr>

                    <div class="x_content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Images</th>
                                <th>Name</th>
                                <th> Designation </th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php $i = 1; @endphp
                            @foreach($team as $teams)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>
                                        <img width="100" src="{{asset('uploads/'. $teams->image)}}" alt="">
                                    </td>
                                    <td>{{ $teams->name }}</td>
                                    <td>{{ substr($teams->designation, 0,50) }}</td>
                                    <td >
                                        <div class="btn-group  btn-group-sm" style="color: white">
                                            <button class="btn btn-success" type="button"> <a href="{{ url('/admin/about/our_team/show/'. $teams->id) }}" style="color: white"><i class="fa fa-eye"></i></a></button>
                                            <button class="btn btn-primary" type="button"><a href="{{ url('/admin/about/our_team/edit/'. $teams->id) }}" style="color: white"><i class="fa fa-edit"></i></a></button>
                                            <button class="btn btn-danger" type="button"><a href="{{ url('/admin/about/our_team/delete/'. $teams->id) }}" style="color: white"><i class="fa fa-trash"></i></a></button>
                                        </div>
                                    </td>
                                </tr>
                                @php $i++ @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection()