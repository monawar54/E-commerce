@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <div id="home-main-content" class="site-home-main-content">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel logo_form">
                    <div class="x_title">
                        <h3>Form Design</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <form action="{{ url('/admin/about/about_us/store') }}" method="post" id="demo-form2" enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Images <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="file" name="image" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Description :<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea name="description" rows="8" class="resizable_textarea form-control" placeholder="add product details..."></textarea>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button class="btn btn-primary" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="reset">Reset</button>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection()