@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel logo_form">
                <div class="x_title">
                    <h2>About table </h2>
                    <div class="clearfix"></div>
                </div>

                <br />
                <hr>

                <div class="x_content">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Images</th>
                            <th> Description </th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @php $i = 1; @endphp
                        @foreach($abouts as $about)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    <img width="100" src="{{asset('uploads/'. $about->image)}}" alt="">
                                </td>
                                <td>{{ substr($about->description, 0,50) }}</td>
                                <td >
                                    <div class="btn-group  btn-group-sm" style="color: white">
                                        <button class="btn btn-success" type="button"> <a href="{{ url('/admin/about/about_us/show/'. $about->id) }}" style="color: white"><i class="fa fa-eye"></i></a></button>
                                        <button class="btn btn-primary" type="button"><a href="{{ url('/admin/about/about_us/edit/'. $about->id) }}" style="color: white"><i class="fa fa-edit"></i></a></button>
                                        <button class="btn btn-danger" type="button"><a href="{{ url('/admin/about/about_us/delete/'. $about->id) }}" style="color: white"><i class="fa fa-trash"></i></a></button>
                                    </div>
                                </td>
                            </tr>
                            @php $i++ @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection()