
@extends('admin.layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data Tables
                <small>advanced tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data tables</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Data Table With Full Features</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Quantity</th>
                                    <th>Size</th>
                                    <th>Color</th>
                                    <th>Price</th>
                                    <th>Old Price</th>
                                    <th>Av_size</th>
                                    <th>Av_color</th>
                                    <th>Stock</th>
                                    <th>pro_ava</th>
                                    <th>Dis_ava</th>
                                    <th>Tags</th>
                                    <th>Brand</th>
                                    <th>Delivery</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i = 1; @endphp
                                @foreach($data as $product)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td class="td_name">{{ substr($product->name, 0,05) }}</td>
                                        <td>{{ substr($product->description, 0,10) }}</td>
                                        <td>
                                            <img width="60" src="{{asset('uploads/'.$product->image)}}" alt="{{ $product->name}}">
                                        </td>
                                        <td>{{$product->quantity}}</td>
                                        <td>{{$product->size}}</td>
                                        <td>{{$product->color}}</td>
                                        <td>{{$product->price}}</td>
                                        <td>{{$product->old_price}}</td>
                                        <td>{{$product->available_size}}</td>
                                        <td>{{$product->available_color}}</td>
                                        <td>{{$product->stock}}</td>
                                        <td>{{$product->pro_available}}</td>
                                        <td>{{$product->dis_available}}</td>
                                        <td>{{$product->tags}}</td>
                                        <td>{{$product->brand}}</td>
                                        <td>{{$product->delivery}}</td>
                                        <td >
                                            <div class="btn-group  btn-group-sm btn-style" style="color: white">
                                                <button class="btn btn-success" type="button"> <a href="{{ url('/admin/news/show/' . $product->id) }}" style="color: white"><i class="fa fa-eye"></i></a></button>
                                                <button class="btn btn-primary button_margin" type="button"><a href="{{ url('/admin/news/edit/'. $product->id) }}" style="color: white"><i class="fa fa-edit"></i></a></button>
                                                <button class="btn btn-danger button_margin" type="button"><a href="{{ url('/admin/news/delete/'. $product->id) }}" style="color: white"><i class="fa fa-trash"></i></a></button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection()