@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <div class="row" >
            <div class="right_col" role="main">
                <div class="container">
                    <div class="clearfix"></div>
                    <div class="row">
                        <h2 style="padding-left: 15px">E-commerce:: product page </h2>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="product-image">
                                            <img src="{{ asset('uploads/'. $data->image) }}" alt="...">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12" style="border:0px solid #e5e5e5;">

                                        <h3 class="prod_title">{{ $data->name }}</h3>

                                        <p>{{ $data->description }}</p>
                                        <br />

                                        <div class="">
                                            <h2>Available Colors</h2>
                                            <ul class="list-inline prod_color">
                                                <li>
                                                    <p>Green</p>
                                                    <div class="color bg-green"></div>
                                                </li>
                                                <li>
                                                    <p>Blue</p>
                                                    <div class="color bg-blue"></div>
                                                </li>
                                                <li>
                                                    <p>Red</p>
                                                    <div class="color bg-red"></div>
                                                </li>
                                                <li>
                                                    <p>Orange</p>
                                                    <div class="color bg-orange"></div>
                                                </li>
                                            </ul>
                                        </div>
                                        <br>

                                        <div class="">
                                            <h2>Size <small>Please select one</small></h2>
                                            <ul class="list-inline prod_size">
                                                <li>
                                                    <button type="button" class="btn btn-default btn-xs">Small</button>
                                                </li>
                                                <li>
                                                    <button type="button" class="btn btn-default btn-xs">Medium</button>
                                                </li>
                                                <li>
                                                    <button type="button" class="btn btn-default btn-xs">Large</button>
                                                </li>
                                                <li>
                                                    <button type="button" class="btn btn-default btn-xs">Xtra-Large</button>
                                                </li>
                                            </ul>
                                        </div>
                                        <br>

                                        <div class="">
                                            <div class="product_price">
                                                <h1 class="price">Price: </h1>
                                                <span class="price-tax">{{ $data->price }}</span>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

    <!-- /page content -->
@endsection