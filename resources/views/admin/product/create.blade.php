@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">

        @if(session()->has('message'))
            {{ session('message') }}
        @endif

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel product_form">
                    <div class="row">
                        <div class="x_title">
                            <h3>Insert Product information... </h3>

                        </div>
                        <hr>
                        <div class="x_content">
                            <br>
                            <form action="{{ url('/admin/product/store') }}" method="POST" id="demo-form2" data-parsley-validate="" enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate="">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name :<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="first-name" name="name" required="required" class="form-control col-md-7 col-xs-12" placeholder="add product name...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Description :<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="description" rows="8" class="resizable_textarea form-control" placeholder="add product details..."></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Images :<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="image" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Category :<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        @foreach($data as $categories)
                                            <label>
                                                <input type="checkbox" name="category_id[]" value="{{$categories->id }}" class="flat" />{{ $categories->catName }}
                                            </label>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Quantity :<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{--<form action="#" method="POST">--}}
                                        <input type="number" value="" name="quantity" class="plus-minus-box" placeholder="add product quantity...">
                                        {{--</form>--}}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Size :<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       {{-- <select name="size">
                                            <option>S</option> <option>M</option> <option>L</option> <option>XL</option> <option>XXL</option>
                                        </select>--}}
                                        <input type="text"  name="size" required="required" class="form-control col-md-7 col-xs-12" placeholder="add product size...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Color :<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="tags_1" type="text" name="color" class="tags form-control" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Price :<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" id="number" name="price" required="required" class="form-control col-md-7 col-xs-12" placeholder="add product price...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Old Price :{{--<span class="required">*</span>--}}
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" id="number" name="old_price" required="required" class="form-control col-md-7 col-xs-12" placeholder="add product old price...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Available Size :{{--<span class="required">*</span>--}}
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="available_size" required="required" class="form-control col-md-7 col-xs-12" placeholder="add available size...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Available Color :{{--<span class="required">*</span>--}}
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="available_color" required="required" class="form-control col-md-7 col-xs-12" placeholder="add available color...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Stock :{{--<span class="required">*</span>--}}
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="stock" required="required" class="form-control col-md-7 col-xs-12" placeholder="add product stock availability...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Available :{{--<span class="required">*</span>--}}
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="pro_available" required="required" class="form-control col-md-7 col-xs-12" placeholder="add product availability...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Discount Available :{{--<span class="required">*</span>--}}
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="dis_available" required="required" class="form-control col-md-7 col-xs-12" placeholder="add product discount availability...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tags :<span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="tags" required="required" class="form-control col-md-7 col-xs-12" placeholder="add product tags...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Brand :{{--<span class="required">*</span>--}}
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="brand" required="required" class="form-control col-md-7 col-xs-12" placeholder="add product brand...">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Delivery :{{--<span class="required">*</span>--}}
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="delivery" required="required" class="form-control col-md-7 col-xs-12" placeholder="add product delivery...">
                                    </div>
                                </div>

                                <hr />

                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-primary" type="button">Cancel</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                                <br />
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection()