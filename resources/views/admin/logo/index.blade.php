@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <div class="">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel logo_form">
                    <div class="x_title">
                        <h2>Logo table </h2>
                        <div class="clearfix"></div>
                    </div>

                    <br />
                    <hr>

                    <div class="x_content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th> Title </th>
                                <th>Images</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php $i = 1; @endphp
                            @foreach($data as $logo)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{ substr($logo->title, 0,30) }}</td>
                                <td>
                                    <img width="100" src="{{asset('uploads/'.$logo->image)}}" alt="">
                                </td>
                                <td >
                                    <div class="btn-group  btn-group-sm" style="color: white">
                                        <button class="btn btn-success" type="button"> <a href="{{ url('/admin/logo/show/'. $logo->id) }}" style="color: white"><i class="fa fa-eye"></i></a></button>
                                        <button class="btn btn-primary" type="button"><a href="" style="color: white"><i class="fa fa-edit"></i></a></button>
                                        <button class="btn btn-danger" type="button"><a href="{{ url('/admin/logo/delete/'. $logo->id) }}" style="color: white"><i class="fa fa-trash"></i></a></button>
                                    </div>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()