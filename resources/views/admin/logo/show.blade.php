@extends('admin.layouts.master')

@section('content')

    <div class="content-wrapper">
        <div class="right_col" role="main" style="min-height: 1054px;">
            <div class="">

                <div class="page-title">
                    <br />
                    <div class="title_left">
                        <h3>Single Product show show</h3>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_content">
                                <div class="row">
                                    <!-- CONTENT MAIL -->
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="inbox-body">
                                            <div class="mail_heading row">
                                                <div class="col-md-4 col-md-offset-8 text-right">
                                                    <p class="date"> 10 April</p>
                                                </div>
                                                <div class="col-md-12">
                                                    <h4>{{$data->title}} </h4>
                                                </div>
                                            </div>
                                            <div class="attachment">
                                                <img class="img-responsive" src="{{ asset('uploads/'. $data->image) }}" alt="img">
                                            </div>
                                            <br>
                                            <div class="btn-group">
                                                <button class="btn btn-sm btn-primary" type="button"><i class="fa fa-arrow-left"></i> Back</button>
                                                <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Forward"><i class="fa fa-share"></i></button>
                                                <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print"><i class="fa fa-print"></i></button>
                                                <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /CONTENT MAIL -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection