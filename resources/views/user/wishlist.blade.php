@extends('layouts.master')
@section('content')
    <!-- page banner area start -->
    <div class="page-banner">
        <img src="{{asset('uploads/' . $slider->image)}}" alt="Page Banner" />
    </div>
    <!-- page banner area end -->
    <div class="cart-page section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive table-one margin-minus section-padding-bottom">
                        <table class="spacing-table text-center">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Unit Price</th>
                                <th>QTY</th>
                                <th>Subtotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="td-img text-left">
                                    <a href="#"><img src="img/cart/1.jpg" alt="Add Product" /></a>
                                    <div class="items-dsc">
                                        <p><a href="#">Sensor Carbon Jenson GX1 Bike</a></p>
                                    </div>
                                </td>
                                <td>$56.00</td>
                                <td>
                                    <form action="#" method="POST">
                                        <div class="plus-minus">
                                            <a class="dec qtybutton">-</a>
                                            <input type="text" value="02" name="qtybutton" class="plus-minus-box">
                                            <a class="inc qtybutton">+</a>
                                        </div>
                                    </form>
                                </td>
                                <td>$112.00</td>
                                <td><i class="fa fa-trash" title="Remove this product"></i></td>
                            </tr>
                            <tr>
                                <td class="td-img text-left">
                                    <a href="#"><img src="img/cart/1.jpg" alt="Add Product" /></a>
                                    <div class="items-dsc">
                                        <p><a href="#">Sensor Carbon Jenson GX1 Bike</a></p>
                                    </div>
                                </td>
                                <td>$56.00</td>
                                <td>
                                    <form action="#" method="POST">
                                        <div class="plus-minus">
                                            <a class="dec qtybutton">-</a>
                                            <input type="text" value="02" name="qtybutton" class="plus-minus-box">
                                            <a class="inc qtybutton">+</a>
                                        </div>
                                    </form>
                                </td>
                                <td>$112.00</td>
                                <td><i class="fa fa-trash" title="Remove this product"></i></td>
                            </tr>
                            <tr>
                                <td class="td-img text-left">
                                    <a href="#"><img src="img/cart/1.jpg" alt="Add Product" /></a>
                                    <div class="items-dsc">
                                        <p><a href="#">Sensor Carbon Jenson GX1 Bike</a></p>
                                    </div>
                                </td>
                                <td>$56.00</td>
                                <td>
                                    <form action="#" method="POST">
                                        <div class="plus-minus">
                                            <a class="dec qtybutton">-</a>
                                            <input type="text" value="02" name="qtybutton" class="plus-minus-box">
                                            <a class="inc qtybutton">+</a>
                                        </div>
                                    </form>
                                </td>
                                <td>$112.00</td>
                                <td><i class="fa fa-trash" title="Remove this product"></i></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection()