@extends('layouts.master')
@section('content')
    <!-- page banner area start -->
    <div class="page-banner">
        <img src="{{asset('uploads/' . $slider->image)}}" alt="Page Banner" />
    </div>
    <!-- page banner area end -->
    <!-- blog content section start -->
    <section class="blog-area blog-one blog-margin section-padding">
        <div class="container">
            <div class="row">
                <?php $i = 1; ?>
                @foreach($blogs as $blog)
                <div class="col-sm-4">
                    @if($i  % 2 == 1)
                        <div class="blog-item">
                            <div class="blog-img">
                                <a href="{{ asset('user/view_blog/' . $blog->id) }}"><img src="{{ asset('uploads/' . $blog->image) }}" alt="Blog" /></a>
                            </div>
                            <div class="blog-text clearfix">
                                <hr class="line"/>
                                <a href="{{ asset('user/view_blog/' . $blog->id) }}"><h4>{{ substr($blog->title, 0, 27) }}</h4></a>
                                <p><span>15 dec </span></p>
                                <p>{{ substr($blog->description, 0, 200) }} </p>
                                <div class="view-more">
                                    <a class="shop-btn" href="{{ asset('user/view_blog/' . $blog->id) }}">View More</a>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="blog-item middel">
                            <div class="blog-text clearfix">
                                <hr class="line"/>
                                <a href="{{ asset('user/view_blog/' . $blog->id) }}"><h4>{{ substr($blog->title, 0, 27) }}</h4></a>
                                <p><span>15 dec </span></p>
                                <p>{{ substr($blog->description, 0, 200) }} </p>
                                <div class="view-more">
                                    <a class="shop-btn" href="{{ asset('user/view_blog/' . $blog->id) }}">View More</a>
                                </div>
                            </div>
                            <div class="blog-img">
                                <a href="{{ asset('user/view_blog/' . $blog->id) }}"><img src="{{ asset('uploads/' . $blog->image) }}" alt="Blog" /></a>
                            </div>
                        </div>
                    @endif
                </div>
                    <?php $i++ ;?>
                @endforeach
            </div>
            <div class="row margin">
                <?php $i = 1; ?>
                @foreach($blogs as $blog)
                <div class="col-sm-4">
                    @if($i  % 2 == 1)
                        <div class="blog-item">
                            <div class="blog-img">
                                <a href="{{ asset('user/view_blog/' . $blog->id) }}"><img src="{{ asset('uploads/' . $blog->image) }}" alt="Blog" /></a>
                            </div>
                            <div class="blog-text clearfix">
                                <hr class="line"/>
                                <a href="{{ asset('user/view_blog/' . $blog->id) }}"><h4>{{ substr($blog->title, 0, 27) }}</h4></a>
                                <p><span>15 dec </span></p>
                                <p>{{ substr($blog->description, 0, 200) }} </p>
                                <div class="view-more">
                                    <a class="shop-btn" href="{{ asset('user/view_blog/' . $blog->id) }}">View More</a>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="blog-item middel">
                            <div class="blog-text clearfix">
                                <hr class="line"/>
                                <a href="{{ asset('user/view_blog/' . $blog->id) }}"><h4>{{ substr($blog->title, 0, 27) }}</h4></a>
                                <p><span>15 dec </span></p>
                                <p>{{ substr($blog->description, 0, 200) }} </p>
                                <div class="view-more">
                                    <a class="shop-btn" href="{{ asset('user/view_blog/' . $blog->id) }}">View More</a>
                                </div>
                            </div>
                            <div class="blog-img">
                                <a href="{{ asset('user/view_blog/' . $blog->id) }}"><img src="{{ asset('uploads/' . $blog->image) }}" alt="Blog" /></a>
                            </div>
                        </div>
                     @endif
                </div>
                    <?php $i++ ;?>
                @endforeach
            </div>
        </div>
    </section>
    <!-- blog content section end -->
@endsection()