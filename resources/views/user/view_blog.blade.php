@extends('layouts.master')
@section('content')
    <!-- page banner area start -->
    <div class="page-banner">
        <img src="{{asset('uploads/' . $slider->image)}}" alt="Page Banner" />
    </div>
    <!-- page banner area end -->
    <!-- blog content section start -->
    <section class="blog-area sigle-blog blog-margin section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <div class="single-blog-page">
                        <div class="single-blog-img">
                            <img src="{{ asset('uploads/' . $blog->image) }}" alt="Single Blog" />
                        </div>
                        <div class="blog-text">
                            <div class="post-title">
                                <h3>{{ substr( $blog->title, 0 , 50) }}</h3>
                                <p class="date-com"><span>Rakib Hossain</span> | {{ date("F j, Y, g:i a",strtotime($blog->created_at))  }}  |  12 comments</p>
                            </div>
                            <p>{{ substr( $blog->description, 0, 450) }}</p>
                            <div class="italic">
                                <p>{{ substr( $blog->description, 0, 450) }}</p>
                            </div>
                            <p>{{ substr( $blog->description, 0, 450) }}</p>
                            <div class="about-author comments">
                                <h4>19 comments</h4>
                                <div class="autohr-text">
                                    <img alt="" src="{{ asset('frontend/img/about/2.jpg') }}">
                                    <div class="author-des">
                                        <h4><a href="#">Gerald Barnes</a></h4>
                                        <span class="floatright"><a href="#">Reply</a>  /   <a href="#">Hide</a></span>
                                        <span>27 Jun, 2016 at 2:30pm</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas eleifend. Phasellus a felis at est bibenes dum feugiat ut eget eni Praesent et messages in consectetur.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="about-author reply">
                                <div class="autohr-text">
                                    <img alt="" src="{{ asset('frontend/img/about/3.jpg') }}">
                                    <div class="author-des">
                                        <h4><a href="#">Gregory Hernandez</a></h4>
                                        <span class="floatright"><a href="#">Reply</a>  /   <a href="#">Hide</a></span>
                                        <span>27 Jun, 2016 at 2:30pm</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas eleifend. Phasellus a felis at est bibenes dum feugiat ut eget eni Praesent et messages in consectetur.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="about-author">
                                <div class="autohr-text">
                                    <img alt="" src="{{ asset('frontend/img/about/4.jpg') }}">
                                    <div class="author-des">
                                        <h4><a href="#">Gregory Hernandez</a></h4>
                                        <span class="floatright"><a href="#">Reply</a>  /   <a href="#">Hide</a></span>
                                        <span>27 Jun, 2016 at 2:30pm</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan egestas eleifend. Phasellus a felis at est bibenes dum feugiat ut eget eni Praesent et messages in consectetur.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="leave-comment">
                            <h4>leave your review</h4>
                            <form method="post" action="mail.php">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="input-text">
                                            <input type="text" placeholder="Your Name" name="name">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="input-text">
                                            <input type="text" required="" placeholder="Email" name="email">
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-text">
                                            <textarea rows="4" placeholder="Comment" id="comment" name="comment"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="submit-text">
                                            <input type="submit" value="submit comments" name="submit">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- single post end -->
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="sidebar left-sidebar">
                        <div class="category single-side">
                            <div class="sidebar-title">
                                <h3>Shop categories</h3>
                            </div>
                            <ul>
                                <li>
                                    <a href="#">Bikes</a> <span class="floatright">(90)</span>
                                </li>
                                <li>
                                    <a href="#">Components</a> <span class="floatright">(60)</span>
                                </li>
                                <li>
                                    <a href="#">Apparel</a> <span class="floatright">(50)</span>
                                </li>
                                <li>
                                    <a href="#">Accessories</a> <span class="floatright">(30)</span>
                                </li>
                            </ul>
                        </div>
                        <div class="single-side clearfix">
                            <div class="sidebar-title">
                                <h3>RECENT ITEMS</h3>
                            </div>
                            <div class="recent-item">
                                <div class="img">
                                    <a href="product-details.html"><img src="{{ asset('frontend/img/products/side1.png') }}" alt="Resent Item" /></a>
                                </div>
                                <div class="text">
                                    <a href="product-details.html"><h5>Kuat NV 2 Bike Tray</h5></a>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star gray-star"></i>
                                    </div>
                                    <p><span>$170.00</span><del>$170.00</del></p>
                                </div>
                            </div>
                            <div class="recent-item">
                                <div class="img">
                                    <a href="product-details.html"><img src="{{ asset('frontend/img/products/side2.png') }}" alt="Resent Item" /></a>
                                </div>
                                <div class="text">
                                    <a href="product-details.html"><h5>Shimano Pro Wrench</h5></a>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star gray-star"></i>
                                    </div>
                                    <p><span>$170.00</span></p>
                                </div>
                            </div>
                            <div class="recent-item">
                                <div class="img">
                                    <a href="product-details.html"><img src="{{ asset('frontend/img/products/side3.png') }}" alt="Resent Item" /></a>
                                </div>
                                <div class="text">
                                    <a href="product-details.html"><h5>Link Pliers</h5></a>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star gray-star"></i>
                                    </div>
                                    <p><span>$170.00</span></p>
                                </div>
                            </div>
                            <div class="recent-item">
                                <div class="img">
                                    <a href="product-details.html"><img src="{{ asset('frontend/img/products/side4.png') }}" alt="Resent Item" /></a>
                                </div>
                                <div class="text">
                                    <a href="product-details.html"><h5>Cable Cutter</h5></a>
                                    <div class="ratting">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star gray-star"></i>
                                    </div>
                                    <p><span>$120.00</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="single-side">
                            <div class="sidebar-title">
                                <h3>Popular Tags</h3>
                            </div>
                            <div class="tag-btn clearfix">
                                <a href="#">Bike</a>
                                <a href="#">Wheel</a>
                                <a href="#">Aparel</a>
                                <a href="#">GPS</a>
                                <a href="#">Track</a>
                                <a href="#">Accessories</a>
                                <a href="#">Men</a>
                                <a href="#">Women</a>
                                <a href="#">Power</a>
                            </div>
                        </div>
                        <div class="sidebar-banner banner-hover">
                            <a href="#"><img src="{{ asset('frontend/img/banner.jpg') }}" alt="Banner" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- blog content section end -->
    <!-- blog content section start -->
    <section class="blog-area blog-two blog-margin section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 col-text-center">
                    <div class="section-title about-title text-center">
                        <h3>Related Post</h3>
                        <div class="shape">
                            <img alt="Title Shape" src="{{ asset('frontend/img/icon/t-shape.png') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="blog-item">
                        <div class="blog-img">
                            <a href="product-details.html"><img src="{{ asset('frontend/img/blog/1.jpg') }}" alt="Blog" /></a>
                        </div>
                        <div class="blog-text clearfix">
                            <a href="{{ asset('/user/view_blog') }}"><h4>Claritas est etiam processus dynamicus</h4></a>
                            <p class="date-com"><span>Rakib Hossain</span> | jan 17 - 2016  |  12 comments</p>
                            <hr class="line"/>
                            <p>sssNam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam. </p>
                            <div class="view-more">
                                <a class="shop-btn" href="{{ asset('/user/view_blog') }}">Read More</a>
                                <a class="shop-btn" href="#"><i class="fa fa-share-alt"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="blog-item">
                        <div class="blog-img">
                            <a href="product-details.html"><img src="{{ asset('frontend/img/blog/2.jpg') }}" alt="Blog" /></a>
                        </div>
                        <div class="blog-text clearfix">
                            <a href="{{ asset('/user/view_blog') }}"><h4>Claritas est etiam processus dynamicus</h4></a>
                            <p class="date-com"><span>Rakib Hossain</span> | jan 17 - 2016  |  12 comments</p>
                            <hr class="line"/>
                            <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam. </p>
                            <div class="view-more">
                                <a class="shop-btn" href="{{ asset('/user/view_blog') }}">Read More</a>
                                <a class="shop-btn" href="#"><i class="fa fa-share-alt"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="blog-item">
                        <div class="blog-img">
                            <a href="product-details.html"><img src="{{ asset('frontend/img/blog/3.jpg') }}" alt="Blog" /></a>
                        </div>
                        <div class="blog-text clearfix">
                            <a href="{{ asset('/user/view_blog') }}"><h4>Claritas est etiam processus dynamicus</h4></a>
                            <p class="date-com"><span>Rakib Hossain</span> | jan 17 - 2016  |  12 comments</p>
                            <hr class="line"/>
                            <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam. </p>
                            <div class="view-more">
                                <a class="shop-btn" href="{{ asset('/user/view_blog') }}">Read More</a>
                                <a class="shop-btn" href="#"><i class="fa fa-share-alt"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- blog content section end -->
@endsection()