@extends('layouts.master')
@section('content')
    <!-- page banner area start -->
    <div class="page-banner">
        <img src="{{asset('uploads/' . $slider->image)}}" alt="Page Banner" />
    </div>
    <!-- page banner area end -->
    <!-- 404 content section start -->
    <div class="error-content section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="error-img">
                        <img src="{{ asset('frontend/img/error.jpg') }}" alt="404 Page" />
                    </div>
                    <div class="error-text text-center">
                        <h3>Opps, Page Not Found</h3>
                        <p>Please try one of the following page.</p>
                        <a href="/">Return home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- 404 content section end -->
@endsection()