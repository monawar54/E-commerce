@extends('layouts.master')
@section('content')
    <!-- page banner area start -->
    <div class="page-banner">
        <img src="{{asset('uploads/' . $slider->image)}}" alt="Page Banner" />
    </div>
    <!-- page banner area end -->
<section class="product-details section-padding-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="left">
                    <!-- Single-pro-slider Big-photo start -->
                    <div class="large-slider zoom-gallery">
                        <div>
                            <img src="{{asset('uploads/'. $products->image)}}" alt="" />
                            <a href="{{asset('uploads/'. $products->image)}}" title="Product Title"><img src="{{asset('frontend/img/icon/zoom.png')}}" alt="" /></a>
                        </div>
                        <div>
                            <img src="{{asset('uploads/'. $products->image)}}" alt="" />
                            <a href="{{asset('uploads/'. $products->image)}}" title="Product Title 2"><img src="{{asset('frontend/img/icon/zoom.png')}}" alt="" /></a>
                        </div>
                        <div>
                            <img src="{{asset('uploads/'. $products->image)}}" alt="" />
                            <a href="{{asset('uploads/'. $products->image)}}" title="Product Title 3"><img src="{{asset('frontend/img/icon/zoom.png')}}" alt="" /></a>
                        </div>
                        <div>
                            <img src="{{asset('uploads/'. $products->image)}}" alt="" />
                            <a href="{{asset('uploads/'. $products->image)}}" title="Product Title"><img src="{{asset('frontend/img/icon/zoom.png')}}" alt="" /></a>
                        </div>
                        <div>
                            <img src="{{asset('uploads/'. $products->image)}}" alt="" />
                            <a href="{{asset('uploads/'. $products->image)}}" title="Product Title 2"><img src="{{asset('frontend/img/icon/zoom.png')}}" alt="" /></a>
                        </div>
                    </div>
                    <!-- Single-pro-slider Big-photo end -->

                    <!-- Single-pro-slider Small-photo start -->
                    <div class="thumb-slider slider-nav">
                        <div>
                            <img src="{{asset('uploads/'. $products->image)}}" alt="" />
                        </div>
                        <div>
                            <img src="{{asset('uploads/'. $products->image)}}" alt="" />
                        </div>
                        <div>
                            <img src="{{asset('uploads/'. $products->image)}}" alt="" />
                        </div>
                        <div>
                            <img src="{{asset('uploads/'. $products->image)}}" alt="" />
                        </div>
                        <div>
                            <img src="{{asset('uploads/'. $products->image)}}" alt="" />
                        </div>
                    </div>
                    <!-- Single-pro-slider Small-photo end -->
                </div>
            </div>
            <div class="col-sm-6">
                <div class="right">
                    <div class="singl-pro-title">
                        <h3>{{$products->name}} </h3>
                        <h1>{{$products->price}}</h1>
                        <hr />
                        <p>{{$products->description}}.</p>
                        <hr />
                        <div class="color-brand clearfix">
                            <div class="s-select">
                                <div class="custom-select">
                                    <select class="form-control">
                                        <option>Color</option>
                                        <option>Red </option>
                                        <option>Green </option>
                                        <option>Blue</option>
                                    </select>
                                </div>
                            </div>
                            <div class="s-select">
                                <div class="custom-select">
                                    <select class="form-control">
                                        <option>Brend</option>
                                        <option>Men </option>
                                        <option>Fashion </option>
                                        <option>Shirt</option>
                                    </select>
                                </div>
                            </div>
                            <div class="s-select s-plus-minus">
                                <form action="#" method="POST">
                                    <div class="plus-minus">
                                        <a class="dec qtybutton">-</a>
                                        <input type="text" value="02" name="qtybutton" class="plus-minus-box">
                                        <a class="inc qtybutton">+</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="actions-btn">
                            <ul class="clearfix text-center">
                                <li>
                                    <a href="cart.html"><i class="fa fa-shopping-cart"></i> add to cart</a>
                                </li>
                                <li>
                                    <a href="cart.html"><i class="fa fa-heart-o"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-compress"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-share-alt"></i></a>
                                </li>
                            </ul>
                        </div>
                        <hr />
                        <div class="categ-tag">
                            <ul class="clearfix">
                                <li>
                                    CATEGORIES:
                                    <a href="#">Bike,</a> <a href="#">Cycle,</a> <a href="#">Ride,</a> <a href="#">Mountain</a>
                                </li>
                                <li>
                                    TAG:
                                    <a href="#">{{$products->tags}}</a>

                                </li>
                            </ul>
                        </div>
                        <div class="specific-pro">
                            <ul>
                                <li class="specific-pro-title">
                                    Product Specifications
                                </li>
                                <li>
                                    <span>Frame</span>
                                    <p>Optimized Construction carbon frame with 130mm travel.</p>
                                </li>
                                <li>
                                    <span>Fork</span>
                                    <p>RockShox Revelation RLT 27.5, 130mm, 15mm thru</p>
                                </li>
                                <li>
                                    <span>Rear Shock</span>
                                    <p>Fox Float CTD Boost Valve rear shock with Coat</p>
                                </li>
                                <li>
                                    <span>Headset</span>
                                    <p>Cane Creek 40</p>
                                </li>
                                <li>
                                    <span>Shifters</span>
                                    <p>SRAM GX 11 Speed Trigger Shifter</p>
                                </li>
                                <li>
                                    <span>Front Derailleur</span>
                                    <p>n\a</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="pro-des-tab">
                    <div class="tab-menu">
                        <ul>
                            <li class="active">
                                <a data-toggle="tab" href="#des">PRODUCT DESCRIPTION</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#reviews">REVIEWS</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tags">TAGS</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#size">SIZE GUIDE</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#custom">CUSTOM TAB</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content margin50">
                        <div class="tab-pane fade in active" id="des">
                            <div class="des-text">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero.</p><br />
                                <p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                            </div>
                        </div>
                        <div class="tab-pane fade in single-blog-page" id="reviews">
                            <div class="leave-comment">
                                <h4>leave your review</h4>
                                <form action="mail.php" method="post">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-text">
                                                <input type="text" name="name" placeholder="Your Name">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="input-text">
                                                <input type="text" name="email" placeholder="Email" required="">
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="input-text">
                                                <textarea name="comment" id="comment" placeholder="Comment" rows="4"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="submit-text">
                                                <input type="submit" name="submit" value="submit comments">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade in" id="tags">
                            <div class="tag-btn clearfix">
                                {{--@foreach($tags as $tag)--}}
                                    <a href="#">TAG{{--{{ $tag->tagName }}--}}</a>
                                {{--@endforeach--}}
                            </div>
                        </div>
                        <div class="tab-pane fade in" id="size">
                            <div class="size-btn clearfix">
                                <a href="#">X</a>
                                <a href="#">m</a>
                                <a href="#">l</a>
                                <a href="#">Xm</a>
                                <a href="#">xxl</a>
                            </div>
                        </div>
                        <div class="tab-pane fade in" id="custom">
                            <div class="des-text">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero.</p><br />
                                <p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <!-- product details area end -->

    <!-- related product section start -->
    <section class="related-area riding-one">
    <div class="container">
        <div class="related-title">
            <h5>Related Products</h5>
        </div>
        <div class="row text-center margin50">
            <div class="related-slider single-products">
               {{-- @php $i = 1; @endphp
                @foreach($product as $products)--}}
                <div class="related-item col-xs-12">
                    <div class="product-item">
                        <div class="pro-img">
                            <a href="product-details.html"><img src="{{--{{asset('uploads/' . $products->image)}}--}}" alt="Product" /></a>
                            <div class="tag-n-s">
                                <span>New</span>
                            </div>
                        </div>
                        <div class="riding-title clearfix">
                            <div class="product-title text-left floatleft">
                                <a href="#"><h5>sdfsd{{--{{ $products->name }}--}}</h5></a>
                                <div class="ratting">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star gray-star"></i>
                                </div>
                                <p><span>$222{{--{{ $products->price }}--}} </span> <del>$175</del></p>
                            </div>
                        </div>
                    </div>
                </div>
               {{-- @php $i++;
                   if ($i== 6)
                    {
                      echo '</div>  <div class="product-item">';
                      break ;
                    }
                @endphp
                @endforeach--}}
            </div>
        </div>
    </div>
</section>
    <!-- related product section end -->

@endsection()