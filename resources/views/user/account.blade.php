@extends('layouts.master')
@section('content')
    <!-- page banner area start -->
    <div class="page-banner">
        <img src="{{asset('uploads/' . $slider->image)}}" alt="Page Banner" />
    </div>
    <!-- page banner area end -->
    <section class="account-page section-padding">
        <div class="container">
            <div class="account-title">
                <h3>My Account</h3>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="single-check m-bottom50">
                        <div class="row">
                            <form action="mail.php" method="post">
                                <div class="single-input clearfix">
                                    <div class="col-xs-12">
                                        <div class="check-title">
                                            <h3>Billing Address</h3>
                                            <p>To add a new address, please fill out the form below.</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>First Name:</label>
                                        <div class="input-text">
                                            <input type="text" name="name" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Last Name:</label>
                                        <div class="input-text">
                                            <input type="text" name="name" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Email:</label>
                                        <div class="input-text">
                                            <input type="text" name="email" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Phone:</label>
                                        <div class="input-text">
                                            <input type="text" name="phone" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>Country:</label>
                                        <div class="input-text">
                                            <input type="text" name="country" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>Address:</label>
                                        <div class="input-text">
                                            <input type="text" name="address" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>City/Town:</label>
                                        <div class="input-text">
                                            <input type="text" name="city" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>Additional information:</label>
                                        <div class="input-text">
                                            <textarea name="message" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="submit-text">
                                            <input type="submit" name="submit" value="Save">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="single-check responsive">
                        <div class="order-history check-title">
                            <h3>Order history</h3>
                            <p>Here are the orders you've placed since your account was created.</p>
                            <span>You have not placed any orders.</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="single-check m-bottom50">
                        <div class="row">
                            <form action="mail.php" method="post">
                                <div class="single-input clearfix">
                                    <div class="col-xs-12">
                                        <div class="check-title">
                                            <h3>Your personal information</h3>
                                            <p>Please be sure to update your personal information if it has changed</p>
                                        </div>
                                        <div class="social-title mail-femail">
                                            <span>* Social title</span>
                                            <div class="custom-radio">
                                                <input id="male" type="radio" name="gender" value="Male">
                                                <label for="male"><span>Mr.</span></label>
                                                <input id="female" type="radio" name="gender" value="Female">
                                                <label for="female"><span>Msr.</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>Your Full Name:</label>
                                        <div class="input-text">
                                            <input type="text" name="name" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>Email:</label>
                                        <div class="input-text">
                                            <input type="text" name="email" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="date-birth">
                                            <label>Date of Birth:</label>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="custom-select">
                                                        <select class="form-control">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                            <option>6</option>
                                                            <option>7</option>
                                                            <option>8</option>
                                                            <option>9</option>
                                                            <option>10</option>
                                                            <option>11</option>
                                                            <option>12</option>
                                                            <option>13</option>
                                                            <option>14</option>
                                                            <option>15</option>
                                                            <option>16</option>
                                                            <option>17</option>
                                                            <option>18</option>
                                                            <option>19</option>
                                                            <option>20</option>
                                                            <option>21</option>
                                                            <option>22</option>
                                                            <option>23</option>
                                                            <option>24</option>
                                                            <option>25</option>
                                                            <option>26</option>
                                                            <option>27</option>
                                                            <option>28</option>
                                                            <option>29</option>
                                                            <option>30</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="custom-select">
                                                        <select class="form-control">
                                                            <option>January</option>
                                                            <option>February </option>
                                                            <option>March </option>
                                                            <option>April </option>
                                                            <option>May </option>
                                                            <option>June </option>
                                                            <option>July </option>
                                                            <option>August </option>
                                                            <option>September </option>
                                                            <option>October </option>
                                                            <option>November </option>
                                                            <option>December </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="custom-select">
                                                        <select class="form-control">
                                                            <option>1990</option>
                                                            <option>1991</option>
                                                            <option>1992</option>
                                                            <option>1993</option>
                                                            <option>1994</option>
                                                            <option>1995</option>
                                                            <option>1996</option>
                                                            <option>1997</option>
                                                            <option>1998</option>
                                                            <option>1999</option>
                                                            <option>2000</option>
                                                            <option>2001</option>
                                                            <option>2002</option>
                                                            <option>2003</option>
                                                            <option>2004</option>
                                                            <option>2005</option>
                                                            <option>2006</option>
                                                            <option>2007</option>
                                                            <option>2008</option>
                                                            <option>2009</option>
                                                            <option>2010</option>
                                                            <option>2011</option>
                                                            <option>2012</option>
                                                            <option>2013</option>
                                                            <option>2014</option>
                                                            <option>2016</option>
                                                            <option>2016</option>
                                                            <option>2017</option>
                                                            <option>2018</option>
                                                            <option>2019</option>
                                                            <option>2020</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>Current Password:</label>
                                        <div class="input-text">
                                            <input type="password" name="password" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>New Password:</label>
                                        <div class="input-text">
                                            <input type="password" name="password" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>Confirmation:</label>
                                        <div class="input-text">
                                            <input type="password" name="password" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="sing-checkbox">
                                            <label>
                                                <input type="checkbox" name="rememberme">
                                                Sign up for our newsletter!
                                            </label>
                                            <label>
                                                <input type="checkbox" name="rememberme">
                                                Receive special offers from our partners!
                                            </label>
                                        </div>
                                        <div class="submit-text">
                                            <input type="submit" name="submit" value="Save">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="single-check">
                        <div class="check-title order-history">
                            <h3>Order history</h3>
                            <p>Here are the orders you've placed since your account was created.</p>
                            <span>You have not placed any orders.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection()