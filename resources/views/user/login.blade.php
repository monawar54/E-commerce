@extends('layouts.master')
@section('content')
    <!-- page banner area start -->
    <div class="page-banner">
        <img src="{{asset('uploads/' . $slider->image)}}" alt="Page Banner" />
    </div>
    <!-- page banner area end -->
    <section class="login-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="single-check">
                            <form action="my-account.html" method="post">
                                <div class="single-input p-bottom50 clearfix">
                                    <div class="col-xs-12">
                                        <div class="check-title">
                                            <h3>login</h3>
                                            <p>If you have an account with us, Please log in!</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>Email:</label>
                                        <div class="input-text">
                                            <input type="text" name="email" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <label>Password:</label>
                                        <div class="input-text">
                                            <input type="password" name="password" />
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="forget">
                                            <a href="#">Forget your password?</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="submit-text">
                                            <input type="submit" name="submit" value="Login">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="single-input p-bottom50 clearfix">
                        <form action="my-account.html" method="post">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="check-title">
                                        <h3>New Customer</h3>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label>First Name:</label>
                                    <div class="input-text">
                                        <input type="text" name="name" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label>Last Name:</label>
                                    <div class="input-text">
                                        <input type="text" name="name" />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <label>Address:</label>
                                    <div class="input-text">
                                        <input type="text" name="address" />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <label>City/Town:</label>
                                    <div class="input-text">
                                        <input type="text" name="city" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label>Email:</label>
                                    <div class="input-text">
                                        <input type="text" name="email" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label>Phone:</label>
                                    <div class="input-text">
                                        <input type="text" name="phone" />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="billing-checkbox">
                                        <input type="checkbox" name="billing-address" value="1" class="checkbox">
                                        <label>Sign up for our newsletter! </label>
                                    </div>
                                    <div class="submit-text">
                                        <input type="submit" name="submit" value="Register">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection()