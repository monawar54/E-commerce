@extends('layouts.master')
@section('content')
    <!-- page banner area start -->
    <div class="page-banner">
        <img src="{{asset('uploads/' . $slider->image)}}" alt="Page Banner" />
    </div>
    <!-- page banner area end -->

    <!-- contact area start -->
    <div class="map-contact clearfix">
        <div class="googleMap-info">
            <div class="map-2" id="googleMap"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="main-contact-info">
                            <ul class="contact-info">
                                <li><h3>contact info</h3></li>
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <div class="text">
                                        <p>777 Seventh Road. Bonosri</p>
                                        <p>Rampura - Dhaka.</p>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <div class="text">
                                        <p>+11 (019) 25184203</p>
                                        <p>+11 (019) 25184203</p>
                                    </div>
                                </li>
                                <li>
                                    <i class="fa fa-paper-plane-o"></i>
                                    <div class="text">
                                        <p>domain@email.com</p>
                                        <p>mail@email.com</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="social-share contact-social">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="#"><i class="fa fa-google"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-behance"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- contact-informaion start -->
    <div class="contact-informaion section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="informaion-text">
                        <h3>Information</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                        <p> Printing and typesetting industry. Lorem Ipsum has been the industry's dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <p> There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="contact-form clearfix">
                            <form action="mail.php" method="post">
                                <div class="col-sm-6">
                                    <div class="input-text">
                                        <input type="text" name="name" placeholder="Your Name" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="input-text">
                                        <input type="text" name="email" placeholder="Email" required />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="input-text">
                                        <input type="text" name="subject" placeholder="Subject" />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="input-text">
                                        <textarea name="message" placeholder="Message" rows="4"></textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="submit-text">
                                        <input type="submit" name="submit" value="submit" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- contact-informaion end -->

@endsection()