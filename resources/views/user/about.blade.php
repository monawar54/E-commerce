@extends('layouts.master')
@section('content')
    <!-- page banner area start -->
    <div class="page-banner">
        <img src="{{asset('uploads/' . $slider->image)}}" alt="Page Banner" />
    </div>
    <!-- page banner area end -->
    <!-- about_us section start -->
    <section class="about-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-6">
                    <div class="about-img">
                        <img src="{{ asset('uploads/' . $about->image) }}" alt="" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="about-text">
                        <h3>About Us</h3>
                        <p> {{ substr($about->description,0,700) }}</p>
                        {{--<p> making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years,</p>--}}
                        <a class="shop-btn" href="#">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- about_us section end -->

    <!-- choose section start -->
    <section class="choose-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 col-text-center">
                    <div class="section-title about-title text-center">
                        <h3>Why choose us</h3>
                        <div class="shape">
                            <img src="{{ asset('frontend/img/icon/t-shape.png') }}" alt="Title Shape" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <img src="{{ asset('frontend/img/about/bg1.jpg') }}" alt="" />
        <div class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="single-choose">
                            <i class="pe-7s-tools"></i>
                            <h3>Strong Service</h3>
                            <p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="single-choose">
                            <i class="pe-7s-refresh"></i>
                            <h3>Money back guarantee</h3>
                            <p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="single-choose">
                            <i class="pe-7s-help2"></i>
                            <h3>Support provide</h3>
                            <p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
                        </div>
                    </div>
                </div>
                <div class="row margin-top">
                    <div class="col-sm-4">
                        <div class="single-choose">
                            <i class="pe-7s-home"></i>
                            <h3>Home Service</h3>
                            <p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="single-choose">
                            <i class="pe-7s-hammer"></i>
                            <h3>Accessories available</h3>
                            <p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="single-choose">
                            <i class="pe-7s-map-2"></i>
                            <h3>GPS tracker</h3>
                            <p>There are many variati passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- choose section end -->

    <!-- our team section start -->
    <div class="our-team section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 col-text-center">
                    <div class="section-title about-title text-center">
                        <h3>Our Team</h3>
                        <div class="shape">
                            <img src="{{ asset('frontend/img/icon/t-shape.png') }}" alt="Title Shape" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    @php $i = 1; @endphp
                    @foreach($team as $teams)
                    <div class="single-member">
                        <div class="member-img">
                            <img src="{{ asset('uploads/' . $teams->image)}}" alt="Team Member" />
                            <div class="social-share text-center">
                                <div class="link-icon">
                                    <a href="#"><i class="fa fa-link"></i></a>
                                </div>
                                <ul class="clearfix">
                                    <li>
                                        <a href="#"><i class="fa fa-google"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-behance"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <h4>{{ $teams->name }}</h4>
                        <p>{{ $teams->designation }}</p>
                    </div>
                        @php $i++;
                                echo '</div>  <div class="col-xs-12 col-sm-6 col-md-3">';
                        @endphp
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- our team section end -->

@endsection()