@extends('layouts.master')
@section('content')
    <!-- page banner area start -->
    <div class="page-banner">
        <img src="{{asset('uploads/' . $slider->image)}}" alt="Page Banner" />
    </div>
    <!-- page banner area end -->

    <!-- product content section start -->
    <section class="product-content section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="shop-menu clearfix">
                        <div class="left floatleft">
                            <div class="tab-menu view-mode">
                                <a class="grid active" data-toggle="tab" href="#grid"><i class="fa fa-th"></i></a>
                                <a class="list" data-toggle="tab" href="#list"><i class="fa fa-bars"></i></a>
                            </div>
                        </div>
                        <div class="right floatright">
                            <ul>
                                <li>
                                    <div class="custom-select">
                                        <select class="form-control">
                                            <option>12</option>
                                            <option>13 </option>
                                            <option>14 </option>
                                            <option>15</option>
                                            <option>16</option>
                                            <option>17</option>
                                        </select>
                                    </div>
                                    <p>Showing</p>
                                </li>
                                <li>
                                    <div class="custom-select">
                                        <select class="form-control">
                                            <option>Bike</option>
                                            <option>Aikes </option>
                                            <option>Biike </option>
                                            <option>Uike</option>
                                        </select>
                                    </div>
                                    <p>Short By</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="tab-content">
                    <div class="tab-pane fade in active text-center" id="grid">
                        <div class="single-products">
                            @php $i = 1; @endphp
                            @foreach($products as $product)
                                <div class="col-xs-12 col-sm-4">
                                    <div class="product-item margin40">
                                        <div class="pro-img shop_grid">
                                            <a href="{{asset('/user/product_details/'. $product->id)}}"><img src="{{asset('uploads/' . $product->image)}}" alt="Product" /></a>
                                            <div class="tag-n-s">
                                                <span>New</span>
                                            </div>
                                            <div class="actions-btn">
                                                <ul class="clearfix">
                                                    <li>
                                                        <a href="cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="fa fa-heart"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details/'. $product->id)}}"><h5>{{ substr($product->name, 0, 30) }}</h5></a>
                                            <div class="ratting">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star gray-star"></i>
                                            </div>
                                            <p><span>${{ $product->price }}</span><del>${{ $product->old_price }}</del></p>
                                        </div>
                                    </div>
                                </div>
                                @php $i++;
                        if ($i== 13){
                            echo '</div>  <div class="col-xs-12 col-sm-4">';
                            break ;
                        }
                                @endphp
                            @endforeach
                        </div>
                    </div>

                    <div class="list-view tab-pane fade in" id="list">
                        <div class="col-xs-12 col-sm-8 col-md-9">
                            <div class="single-product margin40 clearfix">
                                @php $i = 1; @endphp
                                @foreach($products as $product)
                                    <div class="left">
                                        <div class="product-item margin-top">
                                            <div class="pro-img ">
                                                <a href="product-details.html"><img src="{{asset('frontend/img/products/15.jpg')}}" alt="Product" /></a>
                                                <div class="link-icon zoom-gallery">
                                                    <a href="{{asset('frontend/img/products/15.jpg')}}" ><img src="{{asset('frontend/img/icon/w-zoom.png')}}" alt="Product" /></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">
                                        <div class="product-item">
                                            <div class="product-title">
                                                <a href="product-details.html"><h4>Copenhagen Spitfire Chair</h4></a>
                                                <div class="ratting">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star gray-star"></i>
                                                </div>
                                                <p><span>$1700.00</span><del>$170.00</del></p>
                                                <hr />
                                                <p class="list-p">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas</p>
                                                <hr />
                                            </div>
                                            <div class="actions-btn">
                                                <ul class="clearfix text-center">
                                                    <li>
                                                        <a href="cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="cart.html"><i class="fa fa-heart-o"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="fa fa-compress"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    @php $i++;
                                if ($i== 6){
                                    echo '</div> <div class="single-product margin40 clearfix">';
                                    break ;
                                }
                                    @endphp
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-3">
                            <div class="sidebar left-sidebar">
                                <div class="category single-side">
                                    <div class="sidebar-title">
                                        <h3>Shop categories</h3>
                                    </div>
                                    <ul>
                                        <li>
                                            <a href="#">Bikes</a> <span class="floatright">(90)</span>
                                        </li>
                                        <li>
                                            <a href="#">Components</a> <span class="floatright">(60)</span>
                                        </li>
                                        <li>
                                            <a href="#">Apparel</a> <span class="floatright">(50)</span>
                                        </li>
                                        <li>
                                            <a href="#">Accessories</a> <span class="floatright">(30)</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="p-filter single-side">
                                    <div class="sidebar-title">
                                        <h3>price FILTER</h3>
                                    </div>
                                    <p>Range:  $100.00 - 500.00 </p>
                                    <div id="range-slider"></div>
                                </div>
                                <div class="single-side">
                                    <div class="sidebar-title">
                                        <h3>Sizes</h3>
                                    </div>
                                    <div class="size-btn clearfix">
                                        <a href="#">X</a>
                                        <a href="#">m</a>
                                        <a href="#">l</a>
                                        <a href="#">Xm</a>
                                        <a href="#">xxl</a>
                                    </div>
                                </div>
                                <div class="single-side clearfix">
                                    <div class="sidebar-title">
                                        <h3>RECENT ITEMS</h3>
                                    </div>
                                    <div class="recent-item">
                                        <div class="img">
                                            <a href="product-details.html"><img src="{{asset('frontend/img/products/side1.png')}}" alt="Resent Item" /></a>
                                        </div>
                                        <div class="text">
                                            <a href="product-details.html"><h5>Kuat NV 2 Bike Tray</h5></a>
                                            <div class="ratting">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star gray-star"></i>
                                            </div>
                                            <p><span>$170.00</span><del>$170.00</del></p>
                                        </div>
                                    </div>
                                    <div class="recent-item">
                                        <div class="img">
                                            <a href="product-details.html"><img src="{{asset('frontend/img/products/side2.png')}}" alt="Resent Item" /></a>
                                        </div>
                                        <div class="text">
                                            <a href="product-details.html"><h5>Shimano Pro Wrench</h5></a>
                                            <div class="ratting">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star gray-star"></i>
                                            </div>
                                            <p><span>$170.00</span></p>
                                        </div>
                                    </div>
                                    <div class="recent-item">
                                        <div class="img">
                                            <a href="product-details.html"><img src="{{asset('frontend/img/products/side3.png')}}" alt="Resent Item" /></a>
                                        </div>
                                        <div class="text">
                                            <a href="product-details.html"><h5>Link Pliers</h5></a>
                                            <div class="ratting">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star gray-star"></i>
                                            </div>
                                            <p><span>$170.00</span></p>
                                        </div>
                                    </div>
                                    <div class="recent-item">
                                        <div class="img">
                                            <a href="product-details.html"><img src="{{asset('frontend/img/products/side4.png')}}" alt="Resent Item" /></a>
                                        </div>
                                        <div class="text">
                                            <a href="product-details.html"><h5>Cable Cutter</h5></a>
                                            <div class="ratting">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star gray-star"></i>
                                            </div>
                                            <p><span>$120.00</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="single-side">
                                    <div class="sidebar-title">
                                        <h3>Popular Tags</h3>
                                    </div>
                                    <div class="tag-btn clearfix">
                                        <a href="#">Bike</a>
                                        <a href="#">Wheel</a>
                                        <a href="#">Aparel</a>
                                        <a href="#">GPS</a>
                                        <a href="#">Track</a>
                                        <a href="#">Accessories</a>
                                        <a href="#">Men</a>
                                        <a href="#">Women</a>
                                        <a href="#">Power</a>
                                    </div>
                                </div>
                                <div class="sidebar-banner banner-hover">
                                    <a href="#"><img src="{{asset('frontend/img/banner.jpg')}}" alt="Banner" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="shop-menu clearfix margin-close">
                        <div class="left floatleft">
                            <div class="tab-menu view-mode">
                                <a class="grid active" data-toggle="tab" href="#grid"><i class="fa fa-th"></i></a>
                                <a class="list" data-toggle="tab" href="#list"><i class="fa fa-bars"></i></a>
                            </div>
                        </div>
                        <div class="right floatright text-center">
                            <div class="pagnation-ul">
                                <ul class="clearfix">
                                    <li>
                                        <a href="#">1</a>
                                    </li>
                                    <li>
                                        <a href="#">2</a>
                                    </li>
                                    <li>
                                        <a href="#">3</a>
                                    </li>
                                    <li class="dot-dot">
                                        <a href="#">.....</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-angle-right"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- product content section end -->

@endsection()