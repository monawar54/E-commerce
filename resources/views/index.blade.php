
@extends('layouts.master')
@section('content')
    <!-- slider section start -->
    <div class="slider-area slider-one clearfix">
        <div class="slider" id="mainslider">
            @php $i = 1; @endphp
            @foreach($sliders as $slider)
                <div data-src="{{asset('uploads/' . $slider->image)}}">
                    <div class="d-table">
                        <div class="d-tablecell">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="slide-text">
                                            <h1 class="animated fadeInDown">{{ substr($slider->title, 0,25 ) }}</h1>
                                            <div class="shape animated flipInX">
                                                <img src="{{asset('frontend/img/slider/shape.png')}}" alt="" />
                                            </div>
                                            <h4 class="animated fadeIn">{{ substr( $slider->description, 0, 75) }}</h4>
                                            <a class="shop-btn animated fadeInUp" href="{{asset('/user/shop')}}">Shop Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @php $i++; @endphp
            @endforeach
        </div>
    </div>
    <!-- slider section end -->

    <!-- featured section start -->
    <section class="featured-area featured-one section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-6 col-text-center">
                    <div class="section-title text-center">
                        <h3><span>Featured</span> products</h3>
                        <div class="shape">
                            <img src="{{asset('frontend/img/icon/t-shape.png')}}" alt="Title Shape" />
                        </div>
                        <p>It is a long established fact that a reader will be distracted by the readable content page when looking at its layout.</p>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="featured-slider single-products">
                    @php $i = 1; @endphp
                    @foreach($products_all as $product)
                        <div class="single-slide">
                            <div class="padding30">
                                <div class="product-item">
                                    <div class="pro-img ">
                                        <a href="{{asset('/user/product_details/'. $product->id)}}">
                                            <img height="245" width="435" src="{{asset('uploads/' . $product->image)}}" alt="" />
                                        </a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="{{asset('/user/product_details/'. $product->id)}}" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="product-title">
                                        <a href="{{asset('/user/product_details/'. $product->id)}}"><h5>{{ substr($product->name, 0, 25) }}</h5></a>
                                        <p>Price :  <span>${{ $product->price }}</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @php $i++;
                        if ($i== 6){
                            echo '</div>  <div class="single-slide">';
                            break ;
                        }
                    @endphp
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- featured section end -->
    <!-- sell-up section start -->
    <section class="sell-up-area sell-up-one">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 ">
                    <div class="shadow-l-r">
                        <div class="sell-up-left">
                            <h4>For summer Ride</h4>
                            <h2>SALE UP <strong>40%</strong></h2>
                            <hr class="line"/>
                            <ul class="clearfix">
                                <li>
                                    <span></span> With full custom accessories
                                </li>
                                <li>
                                    <span></span> Power booster gamming GPS support
                                </li>
                                <li>
                                    <span></span> Tube less tyre for batter riding
                                </li>
                                <li>
                                    <span></span> Capable strong tire & metal body
                                </li>
                            </ul>
                            <a class="shop-btn" href="{{asset('/user/shop')}}">Shop Now</a>
                    </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-offset-1 col-md-7">
                    <div class="sell-up-right">
                        @php $i = 1; @endphp
                        @foreach($products_all as $product)
                        <div class="single-sellup">
                            <a href="{{asset('/user/product_details/'. $product->id)}}"><img src="{{asset('uploads/' . $product->image)}}" alt="" /></a>
                        </div>
                            @php $i++;
                    if ($i== 5){
                        echo '</div>  <div class="single-sellup">';
                        break ;
                    }
                            @endphp
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- sell-up section end -->
    <!-- riding section start -->
    <section class="riding-area riding-one section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-6 col-text-center">
                    <div class="section-title text-center">
                        <h3><span>riding</span> accessories</h3>
                        <div class="shape">
                            <img src="{{asset('frontend/img/icon/t-shape.png')}}" alt="Title Shape" />
                        </div>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="four-item riding-slider single-products">
                    @php $i = 1; @endphp
                    @foreach($products_all as $product)
                    <div class="riding-item col-xs-12">
                        <div class="product-item">
                            <div class="pro-img riding_img">
                                <a href="{{asset('/user/product_details/'. $product->id)}}"><img src="{{asset('uploads/' . $product->image)}}" alt="Product" /></a>
                                <div class="link-icon">
                                    <a href="{{asset('/user/product_details/'. $product->id)}}"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="riding-title clearfix">
                                <div class="product-title text-left floatleft">
                                    <a href="{{asset('/user/product_details/'. $product->id)}}"><h5>Giro Cipher Helmet</h5></a>
                                    <p><span>$150</span></p>
                                </div>
                                <div class="actions-btn floatright">
                                    <ul class="clearfix">
                                        <li>
                                            <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        @php $i++;
                        if ($i== 6){
                            echo '</div>  <div class="riding-item col-xs-12">';
                            break ;
                        }
                        @endphp
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- riding section end -->
    <!-- discount section start -->
    <section class="discount-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="discount-left">
                        <h4>Promotional</h4>
                        <h2>Discount <strong>UP TO 35%</strong></h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form... or randomised words</p>
                        <a class="shop-btn" href="{{asset('/user/shop')}}">view more</a>
                        <div class="count-text clearfix">
                            <ul id="countdown-1">
                                <li>
                                    <span class="days timenumbers">00</span>
                                    <p class="timeRefDays timedescription">days</p>
                                </li>
                                <li>
                                    <span class="hours timenumbers">00</span>
                                    <p class="timeRefHours timedescription">hrs</p>
                                </li>
                                <li>
                                    <span class="minutes timenumbers">00</span>
                                    <p class="timeRefMinutes timedescription">mins</p>
                                </li>
                                <li>
                                    <span class="seconds timenumbers">00</span>
                                    <p class="timeRefSeconds timedescription">secs</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="discount-right">
                        <img src="{{asset('uploads/' . $discount->image)}}" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- discount section end -->
    <!-- best-sell section start -->
    <section class="best-sell-area best-sell-one section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-6 col-text-center">
                    <div class="section-title text-center">
                        <h3><span>best</span> sell</h3>
                        <div class="shape">
                            <img src="{{asset('frontend/img/icon/t-shape.png')}}" alt="Title Shape" />
                        </div>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="tab-menu">
                        <ul>
                            <li class="active">
                                <a data-toggle="tab" href="#bikes">Bikes <span>-</span></a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#parts">Parts <span>-</span></a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#man">Man <span>-</span></a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#women">Women <span>-</span></a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#accessories">Accessories</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="text-center tab-content">
                    <div class="tab-pane fade in active" id="bikes">
                        <div class="four-item single-products">
                            @php $i = 1; @endphp
                            @foreach($products_all as $product)
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img sell_img">
                                        <a href="{{asset('/user/product_details/'. $product->id)}}"><img src="{{asset('uploads/'. $product->image)}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="{{asset('/user/product_details/'. $product->id)}}" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details/'. $product->id)}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>${{ $product->price }} </span><del>${{ $product->old_price }}</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @php $i++;
                        if ($i== 6){
                            echo '</div> <div class="product-item">';
                            break ;
                        }
                                @endphp
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane fade" id="parts">
                        <div class="four-item single-products">
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="man">
                        <div class="four-item single-products">
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="women">
                        <div class="four-item single-products">
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="accessories">
                        <div class="four-item single-products">
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="product-item">
                                    <div class="pro-img">
                                        <a href="{{asset('/user/product_details')}}"><img src="{{asset('frontend/img/products/b1.png')}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="riding-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details')}}"><h5>T Mounten bike S900</h5></a>
                                            <p>Price: <span>$1500 </span><del>$1800</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="view-more">
                        <a class="shop-btn" href="{{asset('/user/shop')}}">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- best-sell section end -->
    <!-- arrival section start -->
    <section class="arrival-area arrival-one section-padding-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-6 col-text-center">
                    <div class="section-title text-center">
                        <h3><span>New</span> Arrival</h3>
                        <div class="shape">
                            <img src="{{asset('frontend/img/icon/t-shape.png')}}" alt="Title Shape" />
                        </div>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid arrival-content">
            <div class="row text-center">
                <div class="col-xs-12 col-md-6 col-lg-4 arrival-left">
                    <div class="single-arrival left">
                        <div class="product-item">
                            <div class="pro-img arrival-big-img">
                                <a href="{{asset('/user/product_details/' . $category_men->id)}}"><img src="{{asset('uploads/' . $category_men->image)}}" alt="Product" /></a>
                            </div>
                            <div class="tag-new">
                                <img src="{{asset('frontend/img/products/tag.png')}}" alt="New Tag" />
                            </div>
                            <div class="actions-btn">
                                <ul class="clearfix">
                                    <li>
                                        <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="arrival-title clearfix">
                                <div class="product-title">
                                    <a href="{{asset('/user/product_details')}}"><h5>{{ substr($category_men->name,0, 20) }}</h5></a>
                                    <p>Price: <span>${{ $category_men->price }} </span><del>${{ $category_men->old_price }}</del></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 hidden-md col-lg-4 arrival-middle">
                    <div class="single-arrival middel">
                        <div class="row">
                            @php $i = 1; @endphp
                            @foreach($products_all as $product)
                            <div class="col-sm-6">
                                <div class="product-item ">
                                    <div class="pro-img arrival-img">
                                        <a href="{{asset('/user/product_details/' . $product->id)}}"><img src="{{asset('uploads/' . $product->image)}}" alt="Product" /></a>
                                    </div>
                                    <div class="actions-btn">
                                        <ul class="clearfix">
                                            <li>
                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-heart"></i></a>
                                            </li>
                                            <li>
                                                <a href="{{asset('/user/product_details/' . $product->id)}}" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="arrival-title clearfix">
                                        <div class="product-title">
                                            <a href="{{asset('/user/product_details/' . $product->id)}}"><h5>{{ substr($product->name,0,20) }}</h5></a>
                                            <p>Price: <span>{{ $product->price }} </span><del>${{ $product->old_price }}</del></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @php $i++;
                                    if ($i== 5){
                                        echo '</div>  <div class="product-item">';
                                        break ;
                                    }
                                @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xs-12  col-md-6 col-lg-4 arrival-left">
                    <div class="single-arrival right">
                        <div class="product-item">
                            <div class="pro-img arrival-big-img">
                                <a href="{{asset('/user/product_details/' . $category_women->id)}}"><img src="{{asset('uploads/' . $category_women->image)}}" alt="Product" /></a>
                            </div>
                            <div class="tag-new">
                                <img src="{{asset('frontend/img/products/tag.png')}}" alt="New Tag" />
                            </div>
                            <div class="actions-btn">
                                <ul class="clearfix">
                                    <li>
                                        <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#quick-view"><i class="fa fa-eye"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="arrival-title clearfix">
                                <div class="product-title">
                                    <a href="{{asset('/user/product_details')}}"><h5>{{ substr($category_women->name,0,20) }}</h5></a>
                                    <p>Price: <span>${{ $category_women->price }} </span><del>${{ $category_women->old_price }}</del></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- arrival section end -->
    <!-- blog section start -->
    <section class="blog-area blog-one section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-6 col-text-center">
                    <div class="section-title text-center">
                        <h3><span>FROM</span> BLOG</h3>
                        <div class="shape">
                            <img src="{{asset('frontend/img/icon/t-shape.png')}}" alt="Title Shape" />
                        </div>
                        <p>{{ $blog_content->content_title }}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php $i = 1; ?>
                @foreach($blogs as $blog)
                <div class="col-xs-12 col-sm-6 col-md-4">
                    @if($i  % 2 == 1)
                    <div class="blog-item">
                        <div class="blog-img img-size">
                            <a href="{{asset('/user/view_blog/' . $blog->id)}}"><img src="{{asset('uploads/' . $blog->image)}}" alt="Blog" /></a>
                        </div>
                        <div class="blog-text clearfix">
                            <hr class="line"/>
                            <a href="{{asset('/user/view_blog/' . $blog->id)}}"><h4>{{ substr($blog->title,0,27) }}</h4></a>
                            <p><span>15 dec </span></p>
                            <p>{{ substr($blog->description,0,120) }}... </p>
                            <div class="view-more">
                                <a class="shop-btn" href="{{asset('/user/view_blog/' . $blog->id)}}">View More</a>
                            </div>
                        </div>
                    </div>
                    @else
                        <div class="blog-item middel">
                            <div class="blog-text clearfix">
                                <hr class="line"/>
                                <a href="{{asset('/user/view_blog/' . $blog->id)}}"><h4>{{ substr($blog->title,0,27) }}</h4></a>
                                <p><span>15 dec </span></p>
                                <p>{{ substr($blog->description, 0, 185) }} </p>
                                <div class="view-more">
                                    <a class="shop-btn" href="{{asset('/user/view_blog/' . $blog->id)}}">View More</a>
                                </div>
                            </div>
                            <div class="blog-img">
                                <a href="{{asset('/user/view_blog/' . $blog->id)}}"><img src="{{asset('uploads/' . $blog->image)}}" alt="Blog" /></a>
                            </div>
                        </div>
                    @endif
                </div>
                    <?php $i++ ;?>
                @endforeach
            </div>
        </div>
    </section>
    <!-- blog section end -->
    <!-- quick view start -->
    <div class="product-details quick-view modal animated zoomIn" id="quick-view">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="d-table">
                        <div class="d-tablecell">
                            <div class="modal-dialog">
                                <div class="main-view modal-content">
                                    <div class="modal-footer" data-dismiss="modal">
                                        <span>x</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="left">
                                                <!-- Single-pro-slider Big-photo start -->
                                                <div class="quick-img">
                                                    <img src="{{asset('frontend/img/products/l1.jpg')}}" alt="" />
                                                </div>
                                                <!-- Single-pro-slider Big-photo end -->
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="right">
                                                <div class="singl-pro-title">
                                                    <h3>GT Sensor Carbon Jenson </h3>
                                                    <h1>$1700.00</h1>
                                                    <hr />
                                                    <p>doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>
                                                    <hr />
                                                    <div class="color-brand clearfix">
                                                        <div class="s-select">
                                                            <div class="custom-select">
                                                                <select class="form-control">
                                                                    <option>Color</option>
                                                                    <option>Red </option>
                                                                    <option>Green </option>
                                                                    <option>Blue</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="s-select">
                                                            <div class="custom-select">
                                                                <select class="form-control">
                                                                    <option>Brend</option>
                                                                    <option>Men </option>
                                                                    <option>Fashion </option>
                                                                    <option>Shirt</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="s-select s-plus-minus">
                                                            <form action="#" method="POST">
                                                                <div class="plus-minus">
                                                                    <a class="dec qtybutton">-</a>
                                                                    <input type="text" value="02" name="qtybutton" class="plus-minus-box">
                                                                    <a class="inc qtybutton">+</a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="actions-btn">
                                                        <ul class="clearfix text-center">
                                                            <li>
                                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-shopping-cart"></i> add to cart</a>
                                                            </li>
                                                            <li>
                                                                <a href="{{asset('/user/cart')}}"><i class="fa fa-heart-o"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-compress"></i></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"><i class="fa fa-share-alt"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <hr />
                                                    <div class="categ-tag">
                                                        <ul class="clearfix">
                                                            <li>
                                                                CATEGORIES:
                                                                <a href="#">Bike,</a> <a href="#">Cycle,</a> <a href="#">Ride,</a> <a href="#">Mountain</a>
                                                            </li>
                                                            <li>
                                                                TAG:
                                                                <a href="#">Ride,</a> <a href="#">Helmet,</a> <a href="#">cycle,</a> <a href="#">bike</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- quick view end -->
@endsection()