-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table e-commerce.abouts
CREATE TABLE IF NOT EXISTS `abouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.abouts: ~1 rows (approximately)
DELETE FROM `abouts`;
/*!40000 ALTER TABLE `abouts` DISABLE KEYS */;
INSERT INTO `abouts` (`id`, `image`, `description`, `created_at`, `updated_at`) VALUES
	(2, '31ff8eb235.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ut justo vestibulum, varius elit semper, finibus ligula. Etiam ullamcorper semper nisl, eu pellentesque turpis blandit nec. Nullam venenatis aliquam ante sed feugiat. Suspendisse arcu tellus, ultrices at erat ac, ultricies condimentum tellus. Nullam massa felis, feugiat ac blandit sed, posuere imperdiet velit. Ut cursus ultrices ex, eget feugiat tortor volutpat id. Phasellus quis sagittis magna. Sed at velit facilisis, pretium dolor ac, rutrum lectus. Aliquam erat volutpat. Curabitur blandit risus non diam egestas, a dignissim est sollicitudin. Mauris luctus, arcu ut tempus rhoncus, tortor ligula dignissim nunc, sed sodales ipsum purus vel risus. Vestibulum euismod, velit nec ultricies convallis, velit leo eleifend libero, eget pretium ante quam a enim. Quisque quam mi, dictum nec scelerisque id, feugiat nec turpis.\r\n\r\nNunc urna augue, fringilla in bibendum a, interdum eget odio. Vestibulum mollis lacinia justo, sed iaculis tellus scelerisque at. Aliquam vitae accumsan diam. Morbi vel sapien et enim egestas mattis. Sed nec enim in lacus tempus congue. Proin ornare lectus non justo aliquam maximus. Donec efficitur est ac mauris tempus, ac iaculis odio facilisis. Sed eget purus sagittis, eleifend felis non, ullamcorper mauris. Aenean aliquam vel metus et placerat. Praesent dignissim porttitor tortor a sollicitudin.', NULL, NULL);
/*!40000 ALTER TABLE `abouts` ENABLE KEYS */;

-- Dumping structure for table e-commerce.admins
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.admins: ~0 rows (approximately)
DELETE FROM `admins`;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;

-- Dumping structure for table e-commerce.blogs
CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.blogs: ~4 rows (approximately)
DELETE FROM `blogs`;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
INSERT INTO `blogs` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
	(16, 'It is a long established fact that a reader will be distracted by the readable', 'It is a long established fact that a reader will be distracted by the readable It is a long established fact that a reader will be distracted by the readable It is a long established fact that a reader will be distracted by the readable It is a long established fact that a reader will be distracted by the readable', '4e531c6d97.png', NULL, NULL),
	(17, 'more-or-less normal distribution of letters, as opposed to using \'Content here,', 'a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has', 'b760e3c716.png', NULL, NULL),
	(18, 'using \'Content here, more-or-less normal distribution of letters, as opposed to', 'more-or-less normal more-or-less normal distribution of letters, as opposed to using \'Content here, more-or-less normal distribution of letters, as opposed to using \'Content here, distribution of letters, more-or-less normal distribution of letters, as more-or-less normal distribution of letters, as opposed to using \'Content here, opposed to using \'Content here, as opposed to using \'Content here,', '6d104dac25.png', NULL, NULL),
	(19, 'like readable English. Many desktop publishing packages and web page', 'that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will', '0b1048cf78.png', NULL, NULL);
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;

-- Dumping structure for table e-commerce.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catName` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.categories: ~8 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `catName`, `created_at`, `updated_at`) VALUES
	(2, 'Men\'s Fashion', '2018-04-26 15:01:35', '2018-04-26 15:01:39'),
	(3, 'Women\'s Fashion', '2018-04-26 15:01:37', '2018-04-26 15:01:41'),
	(4, 'Accessories', '2018-04-26 15:01:38', '2018-04-26 15:01:42'),
	(5, 'Electronicas', '2018-04-30 15:40:12', '2018-04-30 15:40:38'),
	(6, 'Property', NULL, NULL),
	(7, 'Baby Kids', NULL, NULL),
	(8, 'rgqwgtw', NULL, NULL),
	(9, 'wegWEGwef', NULL, NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table e-commerce.category_product
CREATE TABLE IF NOT EXISTS `category_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_product_category_id_foreign` (`category_id`),
  KEY `category_product_product_id_foreign` (`product_id`),
  CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.category_product: ~5 rows (approximately)
DELETE FROM `category_product`;
/*!40000 ALTER TABLE `category_product` DISABLE KEYS */;
INSERT INTO `category_product` (`id`, `category_id`, `product_id`) VALUES
	(1, 2, 11),
	(2, 2, 12),
	(3, 3, 13),
	(4, 7, 14),
	(6, 5, 16);
/*!40000 ALTER TABLE `category_product` ENABLE KEYS */;

-- Dumping structure for table e-commerce.contents
CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.contents: ~2 rows (approximately)
DELETE FROM `contents`;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` (`id`, `content_title`, `created_at`, `updated_at`) VALUES
	(3, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', '2018-04-26 15:02:00', '2018-04-26 15:02:08');
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;

-- Dumping structure for table e-commerce.logos
CREATE TABLE IF NOT EXISTS `logos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.logos: ~2 rows (approximately)
DELETE FROM `logos`;
/*!40000 ALTER TABLE `logos` DISABLE KEYS */;
INSERT INTO `logos` (`id`, `title`, `image`, `created_at`, `updated_at`) VALUES
	(5, 'ss', '3b0c61664a.png', '2018-04-26 15:02:25', '2018-04-26 15:02:26');
/*!40000 ALTER TABLE `logos` ENABLE KEYS */;

-- Dumping structure for table e-commerce.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.migrations: ~37 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_04_10_041546_create_logos_table', 1),
	(4, '2018_04_10_053228_create_logos_table', 2),
	(5, '2018_04_10_062319_create_logos_table', 3),
	(6, '2018_04_10_063443_create_logos_table', 4),
	(7, '2014_10_12_000000_create_admins_table', 5),
	(8, '2018_04_18_064859_create_products_table', 6),
	(9, '2018_04_18_065131_create_sub_categories_table', 6),
	(10, '2018_04_18_065153_create_categories_table', 6),
	(11, '2018_04_18_065221_create_orders_table', 6),
	(12, '2018_04_18_065238_create_customers_table', 6),
	(13, '2018_04_18_065258_create_discount_table', 6),
	(14, '2018_04_18_065319_create_carts_table', 6),
	(15, '2018_04_18_065333_create_suppliers_table', 6),
	(16, '2018_04_18_065349_create_payment_table', 6),
	(17, '2018_04_18_081621_create_products_table', 7),
	(18, '2018_04_18_082943_create_sub_categories_table', 8),
	(19, '2018_04_18_083350_create_categories_table', 9),
	(20, '2018_04_18_085515_create_p_subcategories_table', 9),
	(21, '2018_04_18_090747_create_cat_subcategories_table', 10),
	(22, '2018_04_18_101556_create_categories_table', 11),
	(23, '2018_04_18_101637_create_sub_categories_table', 11),
	(24, '2018_04_18_102747_create_sub_categories_table', 12),
	(25, '2018_04_22_084004_create_products_table', 13),
	(26, '2018_04_23_035348_create_sub_categories_table', 14),
	(27, '2018_04_23_041919_create_product_subcategories_table', 15),
	(28, '2018_04_23_060414_create_category_product_table', 16),
	(29, '2018_04_24_072527_create_tags_table', 17),
	(30, '2018_04_26_053442_create_contents_table', 18),
	(31, '2018_04_26_060256_create_blogs_table', 19),
	(32, '2018_04_29_110011_create_products_table', 20),
	(33, '2018_04_29_170818_create_sliders_table', 21),
	(34, '2018_05_03_063312_create_abouts_table', 22),
	(35, '2018_05_03_074429_create_abouts_table', 23),
	(36, '2018_05_03_092045_create_teams_table', 24),
	(37, '2018_05_03_093359_create_teams_table', 25);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table e-commerce.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table e-commerce.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `available_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `available_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_available` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dis_available` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.products: ~4 rows (approximately)
DELETE FROM `products`;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `description`, `image`, `quantity`, `size`, `color`, `price`, `old_price`, `available_size`, `available_color`, `stock`, `pro_available`, `dis_available`, `tags`, `brand`, `delivery`, `created_at`, `updated_at`) VALUES
	(11, 'erh', 'erh', '07a5f4eaee.jpg', 1, 'x', 'black', '500', '600', 'XXL', 'Blue', 'Yes', 'No', 'No', 'Men\'s fashion', 'Men', 'Yes', '2018-04-30 05:55:03', '2018-04-30 05:55:03'),
	(12, 'Sed eu nisl mollis, consequat lacus sit amet, convallis dolor.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vulputate mauris at pretium scelerisque. Phasellus ornare nibh id risus faucibus rutrum quis et mauris. Sed eu nisl mollis, consequat lacus sit amet, convallis dolor. Proin id erat eu lorem scelerisque euismod. Praesent et diam nisi. Quisque metus erat, molestie a congue ac, malesuada at metus. Praesent in nisl id justo cursus maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam fringilla odio in interdum blandit. Nullam feugiat tincidunt lorem, quis rutrum urna cursus sit amet.', '6d2de8fe34.jpg', 1, 'XL', 'white', '2000', '2200', 'L,M', 'Black, Blue', 'Yes', 'In stock', 'Yes', 'Men\'s fashion', 'Men', 'Yes', '2018-04-30 09:58:22', '2018-04-30 09:58:22'),
	(13, 'consectetur adipiscing elit. Nulla vulputate mauris at pretium scelerisque.', 'consectetur adipiscing elit. Nulla vulputate mauris at pretium scelerisque. Phasellus ornare nibh id risus faucibus rutrum quis et mauris. Sed eu nisl mollis, consequat lacus sit amet, convallis dolor. Proin id erat eu lorem scelerisque euismod. Praesent et diam nisi. Quisque metus erat, molestie a congue ac, malesuada at metus. Praesent in nisl id justo cursus maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam fringilla odio in interdum blandit. Nullam feugiat tincidunt lorem, quis rutrum urna cursus sit amet.', '839fd908fc.jpg', 2, '5 hat', 'orange', '3000', '3500', '6 hat', 'Blue', 'Yes', 'In stock', 'Yes', 'Women\'s Fashion', 'Women', 'Yes', '2018-04-30 10:17:27', '2018-04-30 10:17:27'),
	(14, 'vulputate vulputate mauris at pretium scelerisque.  mauris at pretium scelerisque.', 'vulputate mauris at pretiumvulputate mauris at pretium scelerisque.  scelerisque. vulputate mauris at vulputate mauris at pretium scelerisque. pretium scelerisque. vulputate mauris at pretium scelerisque.  vulputate mauris at pretium scelerisque. vulputate mauris at pretium scelerisque.', '2649dd3908.jpg', 4, 'small', 'purple', '1800', '2000', 'larg', 'Blue', 'Yes', 'In stock', 'Yes', 'Baby Kids', 'Baby', 'Yes', '2018-04-30 10:23:06', '2018-04-30 10:23:06'),
	(16, 'pretium scelerisque. Phasellus ornare nibh id risus faucibus rutrum quis', 'at pretium scelerisque. Phasellus ornare nibh id risus faucibus rutrum quis et mauris. Sed eu nisl mollis, consequat lacus sit amet, convallis dolor. Proin id erat eu lorem scelerisque euismod. Praesent et diam nisi. Quisque metus erat, molestie a congue ac, malesuada at metus. Praesent in nisl id justo cursus maximus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam fringilla odio in interdum blandit. Nullam feugiat tincidunt lorem, quis rutrum urna cursus sit amet.', 'b31feb518e.jpeg', 3, '5 inch', 'golden', '20000', '22000', '6,5.6', 'black,silver', 'Yes', 'In stock', 'Yes', 'Electronics', 'Electronics', 'Yes', '2018-04-30 11:56:08', '2018-04-30 11:56:08');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table e-commerce.sliders
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.sliders: ~1 rows (approximately)
DELETE FROM `sliders`;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` (`id`, `title`, `description`, `image`, `logo`, `created_at`, `updated_at`) VALUES
	(4, 'best e-commerce site', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', '396080c8d0.jpg', '396080c8d0.jpg', NULL, NULL),
	(5, 'Best musical site', 'established fact that a reader will be distracted by the readable content of a page when looking at its layout.', '1ebb6c1ad4.jpg', '1ebb6c1ad4.jpg', NULL, NULL);
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;

-- Dumping structure for table e-commerce.sub_categories
CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sub_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `sub_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.sub_categories: ~24 rows (approximately)
DELETE FROM `sub_categories`;
/*!40000 ALTER TABLE `sub_categories` DISABLE KEYS */;
INSERT INTO `sub_categories` (`id`, `sub_category`, `category_id`, `created_at`, `updated_at`) VALUES
	(1, 'T-shirt', 2, NULL, NULL),
	(2, 'Burkha', 3, NULL, NULL),
	(3, 'By-cycle', 4, NULL, NULL),
	(5, 'Panjabi', 2, NULL, NULL),
	(6, 'Oppo f1w', 5, NULL, NULL),
	(7, 'Building', 6, NULL, NULL),
	(8, 'pant', 3, NULL, NULL),
	(9, 't-shirt', 3, NULL, NULL),
	(10, 'bike', 4, NULL, NULL),
	(11, 'cycle', 4, NULL, NULL),
	(12, 'grapgics card', 5, NULL, NULL),
	(13, 'plot', 6, NULL, NULL),
	(14, 'samsung', 5, NULL, NULL),
	(15, 'flat', 6, NULL, NULL),
	(16, 'dress', 7, NULL, NULL),
	(18, 'Zin\'s pant', 2, NULL, NULL),
	(19, 'cap', 7, NULL, NULL),
	(20, 'watch', 7, NULL, NULL),
	(21, 'wegwe', 8, NULL, NULL),
	(22, 'WEEEE', 9, NULL, NULL),
	(24, 'sdsdsdsd', 8, NULL, NULL),
	(25, 'ghhgh', 8, NULL, NULL),
	(26, 'bdfhdhdf', 9, NULL, NULL),
	(27, 'sgsgs', 9, NULL, NULL);
/*!40000 ALTER TABLE `sub_categories` ENABLE KEYS */;

-- Dumping structure for table e-commerce.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tagName` varchar(192) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.tags: ~4 rows (approximately)
DELETE FROM `tags`;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` (`id`, `tagName`, `created_at`, `updated_at`) VALUES
	(2, 'Men Fashion', NULL, NULL),
	(3, 'Women Fashion', NULL, NULL),
	(4, 'Accessories', NULL, NULL),
	(5, 'Apparel', NULL, NULL);
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Dumping structure for table e-commerce.teams
CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.teams: ~5 rows (approximately)
DELETE FROM `teams`;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` (`id`, `image`, `name`, `designation`, `created_at`, `updated_at`) VALUES
	(9, 'e48ddef0ab.jpg', 'Fahad Masud', 'Designer', NULL, NULL),
	(10, 'd2fdd5a64f.jpg', 'Imran Hossain', 'WP Developer', NULL, NULL),
	(11, 'cbd2176400.jpg', 'Raju Sheikh', 'Web Developer', NULL, NULL),
	(12, 'fac8811869.jpg', 'Solyman Hossain', 'SEO', NULL, NULL),
	(13, '0aba8b77ae.jpg', 'Habi Jabi', 'Habi jabi', NULL, NULL);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;

-- Dumping structure for table e-commerce.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce.users: ~0 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'raju', 'raju@mail.com', '$2y$10$0srLM45H83ONqZyDAxvvhOsuO/t33nIAYqgilFEbY7zSk5LNXlAMa', 'RS9GiRfJdMBdinY85va1F5CWc9DdEu4tcbrb73xZ1L4LJ8W2yF8xXwwG5PGh', '2018-04-17 08:55:06', '2018-04-17 08:55:06');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
