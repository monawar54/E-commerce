<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*user module*/

/*Route::get('/', 'FrontPageController@slider');*/
Route::get('/', 'FrontPageController@index');
Route::get('/user/blog', 'FrontPageController@blog');
Route::get('/user/view_blog', 'FrontPageController@view_blog');
Route::get('/user/view_blog/{id}', 'FrontPageController@view_blog');
Route::get('/user/login', 'FrontPageController@login');
Route::get('/user/my_account', 'FrontPageController@user_account');
Route::get('/user/checkout', 'FrontPageController@checkout');
Route::get('/user/wishlist', 'FrontPageController@wishlist');
Route::get('/user/cart', 'FrontPageController@cart');
Route::get('/user/about', 'FrontPageController@about');
Route::get('/user/contact', 'FrontPageController@contact');
Route::get('/user/shop', 'FrontPageController@shop');
Route::get('/user/shop/{id}', 'FrontPageController@shop');
Route::get('/user/product_details', 'FrontPageController@product_details');
Route::get('/user/product_details/{id}', 'FrontPageController@product_show');
Route::get('/user/error', 'FrontPageController@error');

/* Admin module */
Route::get('/admin', function () {
    return view('admin.index');
});
//Admin category module
Route::get('/admin/category/create', 'CategoryController@create');
Route::post('/admin/category/create', 'CategoryController@store');
Route::get('/admin/category/index', 'CategoryController@index');

//Admin product module
Route::get('/admin/product/create', 'ProductController@create');
Route::post('/admin/product/store', 'ProductController@store');
Route::get('/admin/product/index', 'ProductController@index');
Route::get('/admin/product/show', 'ProductController@show');
Route::get('/admin/news/show/{id}', 'ProductController@show');
Route::get('/admin/news/delete/{id}', 'ProductController@destroy');

//Admin blog module
Route::get('/admin/blog/create', 'BlogController@create');
Route::get('/admin/blog/index', 'BlogController@index');
Route::post('/admin/blog/store', 'BlogController@store');
Route::get('/admin/blog/delete/{id}', 'BlogController@destroy');
Route::get('/admin/blog/content/create', 'BlogController@content_create');
Route::get('/admin/blog/content/index', 'BlogController@content_index');
Route::post('/admin/blog/content/store', 'BlogController@content_store');
Route::get('/admin/blog/content/delete/{id}', 'BlogController@content_destroy');

//Admin tag module
Route::get('/admin/tag/create', 'TagController@create');
Route::get('/admin/tag/index', 'TagController@index');
Route::post('/admin/tag/store', 'TagController@store');

//Admin logo module
Route::get('/admin/logo/create', 'logoController@create');
Route::post('/admin/logo/create', 'logoController@store');
Route::get('/admin/logo/index', 'logoController@index');
Route::get('/admin/logo/show/{id}', 'logoController@show');
Route::get('/admin/logo/delete/{id}', 'logoController@destroy');

//Admin Slider Module
Route::get('/admin/slider/create', 'SliderController@create');
Route::post('/admin/slider/store', 'SliderController@store');
Route::get('/admin/slider/index', 'SliderController@index');
Route::get('/admin/slider/delete/{id}', 'SliderController@destroy');

//Admin about module
Route::get('/admin/about/about_us/create', 'AboutController@create');
Route::get('/admin/about/about_us/index', 'AboutController@index');
Route::post('/admin/about/about_us/store', 'AboutController@store');
Route::get('/admin/about/about_us/delete/{id}', 'AboutController@destroy');

Route::get('/admin/about/our_team/create', 'TeamController@create');
Route::get('/admin/about/our_team/index', 'TeamController@index');
Route::post('/admin/about/our_team/store', 'TeamController@store');
Route::get('/admin/about/our_team/delete/{id}', 'TeamController@destroy');

//Admin login module
Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/admin', 'AdminController@index');

