<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = DB::table('blogs')->get();
        return view('admin.blog.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('image')){
            $file       = $request->file('image'); // catching inserted file (image)
            $extension  = $file->getClientOriginalExtension(); // Catching image extension
            $filename= substr(md5(time()),0,10).".".$extension;
            $file->move(public_path('\uploads'), $filename); //For move upload file
        } //image uploading for news

        DB::table('blogs')->insert
        (
            [
                'title' => $request->title,
                'description' => $request->description,
                'image' => $filename
            ]
        );

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('blogs')->where('id', $id)->delete();
        return redirect()->back();
    }

    public function content_index()
    {
        $contents = DB::table('contents')->get();
        return view('admin.blog.content.index', compact('contents'));
    }

    public function content_create()
    {
        return view('admin.blog.content.create');
    }

    public function content_store(Request $request)
    {
        DB::table('contents')->insert(
            ['content_title' => $request->content_title]
        );

        return redirect()->back();
    }

    public function content_destroy($id)
    {
        DB::table('contents')->where('id', $id)->delete();
        return redirect()->back();
    }
}
