<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('products')
            ->select('products.*',DB::raw("group_concat(categories.catName) as categories"))
            ->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->leftJoin('categories', 'category_product.category_id', '=', 'categories.id')
            ->groupby('products.id')
            ->get(); // show for all data
        /*$data = DB::table('products')
            ->orderBy('id', 'desc')
            ->get();*/
        return view('admin.product.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = DB::table('categories')->get();
        return view('admin.product.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('image'))
        {
            $file       = $request->file('image'); // catching inserted file (image)
            $extension  = $file->getClientOriginalExtension(); // Catching image extension
            $filename= substr(md5(time()),0,10).".".$extension;
            $file->move(public_path('\uploads'), $filename); //For move upload file
        } //image uploading for news

        //DB::table('products')->insert(
        $data = [
                'name' => $request->name,
                'description' => $request->description,
                'image'  => $filename,
                'quantity' => $request->quantity,
                'size' => $request->size,
                'color' => $request->color,
                'price' => $request->price,
                'old_price' => $request->old_price,
                'available_size' => $request->available_size,
                'available_color' => $request->available_color,
                'stock' => $request->stock,
                'pro_available' => $request->pro_available,
                'dis_available' => $request->dis_available,
                'tags' => $request->tags,
                'brand' => $request->brand,
                'delivery' => $request->delivery

            ];

        if(Product::create($data))
        {
            $products = DB::table('products')->orderBy('created_at', 'desc')->first();
            //dd($products);
            foreach ($request->category_id as $category_id)
            {
                DB::table('category_product')->insert([
                    'category_id' => $category_id,
                    'product_id' => $products->id,
                ]);
            } //mapping table content

            session()->flash('message', 'Data Inserted Successfully'); // message showing
            return redirect('/admin/product/create');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('products')
            ->select('products.*',DB::raw("group_concat(categories.catName) as categories"))
            ->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->leftJoin('categories', 'category_product.category_id', '=', 'categories.id')
            ->groupby('products.id')
            ->orderby('id', 'desc')
            ->where('products.id', $id)
            ->first(); // show for single data

        return view('admin.product.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //DB::table('category_product')->where('product_id', $id)->delete();
        DB::table('products')->where('id', $id)->delete();
        return redirect('/admin/product/index');
    }
}
