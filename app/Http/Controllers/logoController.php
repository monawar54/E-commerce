<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class logoController extends Controller
{
        public function create()
    {
        return view ('admin.logo.create');
    }

    public function index()
    {
        $data = DB::table('logos')
            ->orderBy('id', 'desc')
            ->get();
        return view('admin.logo.index', compact('data'));
    }

    public function store(Request $request)
    {
        if($request->hasFile('image')){
            $file       = $request->file('image'); // catching inserted file (image)
            $extension  = $file->getClientOriginalExtension(); // Catching image extension
            $filename= substr(md5(time()),0,10).".".$extension;
            $file->move(public_path('\uploads'), $filename); //For move upload file
        } //image uploading for news
        DB::table('logos')->insert(
            [
                'title' => $request->title,
                'image'  => $filename
            ]
        );

        return redirect('/admin/logo/create');
    }

        public function show($id)
    {
        $data = DB::table('logos')
            ->where('id', $id)
            ->first();
        return view('admin.logo.show', compact('data'));
    }


    public function destroy($id)
    {
        DB::table('logos')->where('id', $id)->delete();
        return redirect('/admin/logo/index');
    }


}
