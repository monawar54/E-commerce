<?php

namespace App\Http\Controllers;
use App\Category;
use App\About;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class FrontPageController extends Controller
{
    public function index()
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();

        $products_all = DB::table('products')
            ->select('products.*',DB::raw("group_concat(categories.catName) as categories"))
            ->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->leftJoin('categories', 'category_product.category_id', '=', 'categories.id')
            ->groupby('products.id')
            ->orderBy('id', 'desc')
            ->get(); // show for all data

        $discount = DB::table('products')
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();

        $blogs = DB::table('blogs')
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();

        $blog_content = DB::table('contents')
            ->orderBy('id', 'desc')
            ->first();

        $category = Category::with('category')
            ->limit(12)
            ->get(); //using eloquent concept via model


        $category_men = DB::table('products')
            ->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->where('category_product.category_id', '=', '2')
            ->orderBy('product_id','desc')
            ->first(); // OrderedBy category wise news uploaded

        $category_women = DB::table('products')
            ->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->where('category_product.category_id', '=', '3')
            ->orderBy('product_id','desc')
            ->first(); // OrderedBy category wise news uploaded

        return view('index', compact(
            'logo', 'category','products_all',
            'discount','blog_content','blogs', 'sliders',
            'category_men','category_women'));
    }
    public function blog()
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $blogs = DB::table('blogs')
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();

        $category = Category::with('category')->limit(12)->get();

        return view('user.blog', compact('logo','category','blogs','sliders','slider'));
    }
    public function view_blog($id)
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $blog = DB::table('blogs')
            ->where('id', $id)
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();
        $category = Category::with('category')->limit(12)->get();

        return view('user.view_blog', compact('logo','category','blog','sliders','slider'));
    }

    public function login()
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();
        $category = Category::with('category')->limit(12)->get();

        return view('user.login', compact('logo','category','sliders','sliders','slider'));
    }



    public function user_account()
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();
        $category = Category::with('category')->limit(12)->get();

        return view('user.account', compact('logo','category','sliders','sliders','slider'));
    }

    public function checkout()
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();
        $category = Category::with('category')->limit(12)->get();

        return view('user.checkout', compact('logo','category','sliders','slider'));
    }

    public function wishlist()
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();
        $category = Category::with('category')->limit(12)->get();

        return view('user.wishlist', compact('logo','category','sliders','slider'));
    }

    public function cart()
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();
        $category = Category::with('category')->limit(12)->get();

        return view('user.cart', compact('logo','category','sliders','slider'));
    }

    public function about()
    {

        $about = DB::table('abouts')
            ->orderBy('id', 'desc')
            ->first();
        $team = DB::table('teams')
            ->orderBy('id', 'desc')
            ->limit(4)
            ->get();

        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();

        $category = Category::with('category')->limit(12)->get();
        /*$about = About::find('about_us')->first();*/

        return view('user.about', compact('logo','category','sliders','slider','slider','about','team'));
    }

    public function contact()
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();
        $category = Category::with('category')->limit(12)->get();

        return view('user.contact', compact('logo','category','sliders','slider'));
    }

    public function shop()
    {
        $category_shop = DB::table('products')
            ->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->where('category_product.category_id', '=', '2')
            ->orderBy('product_id','desc')
            ->get(); // OrderedBy category wise news uploaded

        $products = DB::table('products')
            ->select('products.*',DB::raw("group_concat(categories.catName) as categories"))
            ->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->leftJoin('categories', 'category_product.category_id', '=', 'categories.id')
            ->groupby('products.id')
            ->orderBy('id', 'desc')
            ->get(); // show for all data

        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();

        $category = Category::with('category')->limit(12)->get();

        return view('user.shop', compact('logo','category','products','sliders','slider','category_shop'));
    }

    public function product_details()
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $products = DB::table('products')
            ->orderBy('id', 'desc')
            ->first();
        $product = DB::table('products')
            ->orderBy('id', 'desc')
            ->get();
        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();
        $category = Category::with('category')->limit(12)->get();

        $tags = DB::table('tags')->get();

        return view('user.product_details', compact('logo','category','product','products','tags','sliders','slider'));
    }
    public function product_show($id)
    {
        $category_men = DB::table('products')
            ->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->where('category_product.category_id', '=', '2')
            ->orderBy('product_id','desc')
            ->first(); // OrderedBy category wise news uploaded

        $category_women = DB::table('products')
            ->leftJoin('category_product', 'products.id', '=', 'category_product.product_id')
            ->where('category_product.category_id', '=', '3')
            ->orderBy('product_id','desc')
            ->first(); // OrderedBy category wise news uploaded

        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $products = DB::table('products')
            ->where('id', $id)
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();

        $category = Category::with('category')->limit(12)->get();

        return view('user.product_details', compact('logo','category','products','sliders','category_men','category_women','slider'));
    }

    public function error()
    {
        $sliders = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->limit(2)
            ->get();
        $slider = DB::table('sliders')
            ->orderBy('id', 'desc')
            ->first();

        $logo = DB::table('logos')
            ->orderBy('id', 'desc')
            ->first();
        $category = Category::with('category')->limit(12)->get();

        return view('user.error404', compact('logo','category','sliders','slider'));
    }
}
