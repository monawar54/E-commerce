<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = [ 'image', 'description' ];

    /*public function about_us(){

        return $this->hasMany('App\About');
    }*/
}
