<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [

        'name',
        'description',
        'image',
        'quantity',
        'size',
        'color',
        'price',
        'old_price',
        'available_size',
        'available_color',
        'stock',
        'pro_available',
        'dis_available',
        'tags',
        'brand',
        'delivery',
    ];
}
