<div class="content-wrapper">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2 style="padding-left: 15px">Single product show</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <div class="row">
                                    <!-- CONTENT MAIL -->
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="inbox-body">
                                            <div class="mail_heading row">
                                                <div class="col-md-4 col-md-offset-8 text-right">
                                                    <p class="date"> {{ date("F j, Y, g:i a",strtotime($data->created_at))  }}</p>
                                                </div>
                                                <div class="col-md-12">
                                                    Product Name :<h3> {{ $data->name }}</h3>
                                                    Product Name :<p>{{ $data->description }}</p>
                                                    Product Name :<h4> {{ $data->quantity }} </h4>
                                                    Product Name :<h4> {{ $data->size }} </h4>
                                                    Product Name :<h4> {{ $data->color }} </h4>
                                                    Product Name :<h4> {{ $data->price }} </h4>
                                                    Product Name :<img class="img-responsive" src="{{ asset('uploads/'. $data->image) }}" alt="img">
                                                </div>
                                            </div>
                                            {{--<div class="attachment">

                                            </div>--}}
                                            <br />
                                            <div class="btn-group">
                                                <button class="btn btn-sm btn-primary" type="button"><i class="fa fa-arrow-left"></i> Back</button>
                                                <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Forward"><i class="fa fa-share"></i></button>
                                                <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Print"><i class="fa fa-print"></i></button>
                                                <button class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                            <hr />
                                        </div>
                                    </div>
                                    <!-- /CONTENT MAIL -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>